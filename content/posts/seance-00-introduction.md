---
title: "Séance 00 - Introduction"
numero: "00"
date: 2022-08-29
date_p: 2022-09-08
bibfile: data/bibliographie.json
visible: true
reference: true
layout: diapositive
toc: false
---
{{< psectioni >}}
## Bienvenue !

{{< pcache >}}
1. Tour de table
2. Déroulement du cours
3. Objectifs du cours
4. Présentation des séances
5. Évaluations et participation
6. Accéder aux ressources
{{< /pcache >}}

{{< pnote >}}
Cette séance est dédiée à la présentation du cours, son déroulement et autres détails utiles.

{{< /pnote >}}

{{< psectiono >}}

{{< psectioni >}}
## 1. Tour de table
3 questions :

- quel est votre nom ?
- pourquoi avez-vous choisi ce livre ?
- qu'attendez-vous de ce cours ?

{{< pnote >}}
Mon nom est Antoine Fauchié, je suis doctorant au Département de littératures et de langues du monde de l'Université de Montréal.
Je suis un vieil étudiant puisque j'ai déjà une (petite) vie professionnelle derrière moi : j'ai accompagné les professionnels du livre en Rhône-Alpes (une région en France), des auteurs aux éditeurs, et j'ai enseigné plusieurs années à l'Université de Grenoble (en sciences de l'information).
Je suis en rédaction de thèse depuis peu, je suis aussi auxiliaire de recherche à la Chaire de recherche du Canada sur les écritures numériques (dirigée par Marcello Vitali-Rosati), je coordonne un projet du Groupe de recherche sur les éditions critiques en contexte numérique et je participe aux projets du Centre de recherche interuniversitaire sur les humanités numériques.
Je vais avoir beaucoup d'occasions de vous parler des projets sur lesquels nous travaillons dans ces laboratoires.

Je donne ce cours parce qu'il m'a été attribué par le Département de littératures et de langues du monde, je suis très heureux d'avoir l'occasion de présenter des initiatives et des projets qui me tiennent à cœur et qui sont parfois en lien avec ma thèse.
À vous !
{{< /pnote >}}

{{< psectiono >}}

{{< psectioni >}}
## 2. Déroulement du cours

- cours en présentiel = présence requise
- lectures **obligatoires** avant chaque séance
- support/ressources disponibles à la fin de chaque cours (site web dédié)
- pendant les séances : partie théorique, manipulations, lectures, échanges
- exposés à prévoir sur certaines séances

{{< pnote >}}
Ni vous ni moi ne pouvons anticiper comment va se passer cet automne, si le cours doit passer en distanciel (c'est peu probable) j'adapterai les séances et le déroulement général.
Mais tant que le cours est prévu en présentiel, vous êtes censé·e·s être présent·e·s à chaque séance.

Pour chaque séance vous devez lire un texte, souvent de quelques pages.
Ce texte servira de base à une discussion pendant la séance, vous aurez par ailleurs besoin de les connaître pour les différents travaux à réaliser.

Tous les supports de cours, y compris une partie de mes notes, seront placés à la suite de chaque séance sur un site web dédié.
Vous pourrez donc les consulter à tout moment.
{{< /pnote >}}

{{< psectiono >}}

{{< psectioni >}}
## [hnu2000.quaternum.net](https://hnu2000.quaternum.net)
{{< psectiono >}}

{{< psectioni >}}
## 3. Objectifs du cours
> Le numérique habite l’ensemble de nos vies et touche aussi, et surtout, à nos activités purement « humanistes », ou même « humaines ».  
> Michael Sinatra et Marcello Vitali-Rosati, "Histoire des humanités numériques" dans _Pratiques de l'édition numérique_

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Objectifs du cours
{{< /pcache >}}

- comprendre les humanités numériques
- découvrir des outils et des méthodes
- apprendre à travailler avec le numérique
- réfléchir à nos conceptions des sciences humaines
- jouer avec des logiciels/applications/programmes

{{< pnote >}}
Pour reprendre la définition et les objectifs de cours tels qu'exprimés :

> Que sont les humanités numériques et quelles technologies sont utilisées dans cette approche scientifique ?
Les "humanités numériques" sont à la fois une méthode scientifique, un programme de recherche ou une approche pluridisciplinaire : elles offrent de nouvelles perspectives pour appréhender, lire et comprendre le monde qui nous entoure.
Depuis l'avènement du numérique, la compréhension des écosystèmes technologiques devient une nécessité dans le champ des sciences humaines.
Le cours HNU 2000 "Humanités numériques : technologies" est une opportunité pour explorer de façon originale les outils théoriques et pratiques utilisés dans les humanités numériques : lire, écrire, chercher, explorer, visualiser, analyser, publier, etc.
Ce cours est un espace de formation pratique et de découverte des méthodes de base en humanités numériques, avec une orientation vers les démarches d'écriture, d'édition et de publication.
>
> Ce cours est une initiation aux technologies utilisées dans les humanités numériques, les étudiant·e·s devront comprendre, explorer, manipuler et expérimenter des outils, des logiciels, des méthodes et des programmes informatiques.
Le cours HNU 1000 "Humanités numériques : théories" est à la fois un prérequis et un compagnon de ce cours pratiques.
>
> À l'issue du cours les étudiant·e·s seront en mesure de comprendre les enjeux technologiques des humanités numériques, de réutiliser des concepts liés aux humanités numériques, d'appréhender des méthodes utilisées dans les humanités numériques et d'utiliser des outils/applications.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Objectifs du cours
{{< /pcache >}}
Quelques moyens pour remplir ces objectifs :

- questionner notre rapport aux numériques
- faire preuve de curiosité
- établir des liens entre les pratiques d'hier et celles d'aujourd'hui

{{< pnote >}}
L'_outillage_ est souvent un aspect mal considéré en humanités, pourtant c'est la condition même de la possibilité de nos recherches : nos outils de collecte, d'enregistrement, de traitement, de prise de notes, de rédaction et de publication sont ce par quoi tous commence.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 4. Présentation des séances

### Séance 01 - 08-09-2022 - Introduction
Séance d'introduction. En détail :

- présentation de l'enseignant ;
- présentation des étudiant·e·s en trois questions ;
- présentation du cours : déroulement, courte présentation des séances, participation, évaluations ;
- comment accéder aux ressources liées au cours ?

{{< pnote >}}
Si les lectures pour chaque séance ne sont pas indiquées, elles le seront au moins une semaine avant.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Présentation des séances
{{< /pcache >}}
### Séance 02 - 15-09-2022 - 10 outils des Humanités numériques
Présentation courte des outils qui seront étudiés durant les séances.

{{< pnote >}}
Cette présentation permettra une compréhension des enjeux autour de ces outils, afin que les étudiant·e·s puissent choisir lesquels choisir pour une présentation orale.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Présentation des séances
{{< /pcache >}}
### Séance 03 - 22-09-2022 - Isidore.science / Chercher de l’information (Internet, moteurs de recherche)

Lecture : {{< cite huma-num_isidore_2021 >}}

Objectifs de la séance : comprendre le fonctionnement d'Internet et du Web, les mécanismes des moteurs de recherche et l'enjeu de disposer d'outils pensés pour la recherche en sciences humaines.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Présentation des séances
{{< /pcache >}}
### Séance 04 - 29-09-2022 - Hypothes.is / Annoter le Web (Open Access, standards) 

Objectifs de la séance : comprendre les rouages du Web et notamment le fonctionnement de HTML, appréhender les enjeux des standards et du libre accès (_Open Access_), apprendre à utiliser Hypothes.is et découvrir des usages des annotations.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Présentation des séances
{{< /pcache >}}
### Séance 05 - 06-10-2022 - Stylo / Écrire sémantiquement (écriture numérique, balisages)
 
Texte à lire : {{< cite "vitali-rosati_ecrire_2020" >}}

Objectifs de la séance : définir ce qu'est l'écriture numérique, appréhender les contraintes de l'édition académique, découvrir les langages de balisage et apprendre à utiliser l'éditeur de texte Stylo.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Présentation des séances
{{< /pcache >}}
### Séance 06 - 13-10-2022 - Zotero / Récolter, organiser et partager des données bibliographiques (bibliographie structurée)
Texte à lire : {{< cite "collectif_pourquoi_2018" >}}

Objectifs de la séance : définir ce qu'est une bibliographie structurée, comprendre les enjeux liés à la gestion des références bibliographiques, apprendre à utiliser Zotero et découvrir une utilisation collective de ce logiciel/service en ligne.

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Présentation des séances
{{< /pcache >}}
### Séance 07 - 20-10-2022 - Évaluation de mi-session
Questionnaire sur table : questions fermées et ouvertes.

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Présentation des séances
{{< /pcache >}}
### Séance 08 - 03-11-2022 - Ngram Viewer / Comparer dans le temps (index, taxonomie)
Texte à lire : {{< cite "bot_contribution_2012" >}}

Objectifs de la séance : introduction à la fouille de texte, découverte de la taxonomie, présentation de projets de numérisation et apprentissage du service en ligne Ngram Viewer.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Présentation des séances
{{< /pcache >}}
### Séance 09 - 10-11-2022 - Voyant Tools / Analyser du texte (logiciel libre, fouille de texte)
Texte à lire : {{< cite "sinclair_e-texts_2020" >}}

Objectifs de la séance : pratique de la fouille de texte, comprendre les enjeux de l'analyse de texte, apprendre à utiliser Voyant Tools et découvrir comment interpréter les résultats.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Présentation des séances
{{< /pcache >}}
### Séance 10 - 17-11-2022 - WordPress / Publier sur le web (WYSIWYG)
Texte à lire : {{< cite "blanchard_ce_2010" >}}

Objectifs de la séance : distinguer WYSIWYG et WYSIWYM, comprendre les enjeux des logiciels _open source_ ou libres, apprendre à utiliser WordPress et à construire un site web.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Présentation des séances
{{< /pcache >}}
### Séance 11 - 24-11-2022 - Gephi / Visualiser des réseaux (logiciel libre, visualisation)
Texte à lire : {{< cite "grandjean_introduction_2015" >}}

Objectifs de la séance : appréhender les enjeux de la visualisation de données dans différents domaines en sciences humaines, comprendre le fonctionnement des licences libres ou ouvertes, apprendre à utiliser le logiciel Gephi.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Présentation des séances
{{< /pcache >}}
### Séance 12 - 01-12-2022 - Timeline.js / Construire une ligne du temps (outils du Web, bibliothèques de code)
Texte à lire : à définir.

Objectifs de la séance : apprendre à utiliser et à jouer avec Timeline.js.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Présentation des séances
{{< /pcache >}}
### Séance 13 - 08-12-2022 - Ed. et GitHub Pages / Publier des contenus sans trop de dépendance (édition numérique, minimal computing, single source publishing)
Texte à lire : {{< cite "gil_design_2019" >}}

Objectifs de la séance : découvrir les enjeux de l'édition numérique, appréhender le concept de minimal computing, comprendre le concept de _single source publishing_ et créer un site web avec GitHub Pages.
{{< psectiono >}}


{{< psectioni >}}
## 5. Évaluations et participation
Rappel :  
1h de cours = 1,5h de travail hors cours

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 5. Évaluations et participation
{{< /pcache >}}
Participation :

- préparation
- concentration
- être actif·ve
- écouter
- créer des connexions

{{< pnote >}}
Si votre présence est requise à chaque séance, nous savons désormais qu'il est difficile de prévoir l'imprévisible.
La présence n'est pas évaluée mais je note qui est présent en classe.
En suivant ce cours vous vous engagez toutefois à venir à chaque séance et à suivre les recommandations suivantes dans la mesure du possible et de votre situation :

- préparation : lectures demandées et relecture du cours ;
- concentration : éviter les distractions pendant les séances ;
- être actif·ve : écouter le cours, participer aux manipulations, poser des questions ;
- écouter : lorsque d'autres étudiant·e·s prennent la parole, écoutez-les ;
- créer des connexions : synthétiser et mettre en rapport les différents contenus présentés.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 5. Évaluations et participation
{{< /pcache >}}

1. examen de mi-session (30 %) : questionnaire lors de la séance du 20 octobre 2022 ;
2. étude de cas sous forme d'exposé (30 %) : cours exposé de 10 minutes en classe pour présenter une méthode ou un outil issu des humanités numériques ;
3. examen de fin de session (40%) : rédaction (environ 1300 mots / 10000 signes) à partir d'un sujet donné.

{{< psectiono >}}

{{< psectioni >}}
## 6. Accéder aux ressources

- la majorité des ressources sont en accès libre (liens hypertextes indiqués)
- pour le reste : PDF sur Studium

{{< pnote >}}
Toutes les références indiquées dans la bibliographie doivent être consultées : cela ne signifie pas que vous devez tout lire, mais que vous devez avoir au moins pris connaissance de l'objet des références (de quoi ça parle) et lu plusieurs passages d'au moins 6 ouvrages/articles.

Lorsque les références sont disponibles en ligne le lien est indiqué à la fois sur Studium, sur le site du cours dédié ou sur la bibliographie dans Zotero.
Attention toutefois, certaines ressources en ligne nécessitent une connexion UdeM : soit depuis l'Université, soit en passant par le VPN.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
## Plagiat et fraude
"Tout acte de plagiat, fraude, copiage, tricherie ou falsification de document commis par une étudiante, un étudiant, de même que toute participation à ces actes ou tentative de les commettre, à l’occasion d’un examen ou d’un travail faisant l’objet d’une évaluation ou dans toute autre circonstance, constituent une infraction. Vous trouverez à l’adresse suivante les différentes formes de fraude et de plagiat ainsi que les sanctions prévues par l’Université : [https://integrite.umontreal.ca](https://integrite.umontreal.ca)"
{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
## Me contacter
Antoine Fauchié : [antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca) ou Studium

**Local C-8041** (presque tous les jours de la semaine)
{{< psectiono >}}
