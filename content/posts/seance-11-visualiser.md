---
title: "Séance 11 - Visualiser"
numero: "11"
date: 2022-08-29
date_p: 2022-11-24
lecture: "Grandjean, M. (2017). _Structures complexes et organisations internationales_. https://halshs.archives-ouvertes.fr/halshs-01610098v2"
layout: diapositive
visible: true
---
{{< psectioni >}}
{{< pcache >}}

## Plan

1. Les graphes : quelques éléments théoriques
2. Gephi : un logiciel libre
3. Découverte et manipulations de Gephi

{{< /pcache >}}
{{< pnote >}}
Cette séance est dédiée à visualisation de données et plus particulièrement aux _graphes_.
Nous ne pourrons pas explorer toutes les visualisations de données existantes, nous allons nous concentrer sur les graphes qui sont une visualisation des réseaux.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 1. Les graphes : quelques éléments théoriques

{{< figure src="graph-base.png" legende="Un exemple de graphe" >}}

{{< pnote >}}
Les graphes sont très utilisés pour comprendre les relations entre des éléments qui constituent par exemple un réseau social (liens entre des personnes), un réseau d'énergie (comme le gaz est distribué depuis la Russie), un réseau aérien (les aéroports reliés par voie aérienne) ou le routage pour les réseaux informatiques (comme Internet).
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}

{{< figure src="submarine-cables.png" legende="Câbles sous-marins" >}}

{{< pnote >}}
Voir https://www.submarinecablemap.com/.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Les graphes : quelques éléments théoriques
{{< /pcache >}}
### 1.1. La théorie des graphes

- **nœuds** ou **sommets** : ce sont des éléments qui sont en relation via des liens ou arêtes ;
- **liens** ou **arêtes** : des paires de sommets, une arête _joint_ des sommets ;
- le dessin d'un graphe : la représentation de relations entre des éléments.

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Les graphes : quelques éléments théoriques
{{< /pcache >}}
### 1.2. Des sommets, des arêtes, des relations

{{< figure src="graph.png" >}}

| | | |
|---|---|---|
|0  |adjacent à |0,1,2,3 |
|1  |adjacent à |0   |
|2  |adjacent à |0,3,4   |
|3  |adjacent à |0,2   |
|4  |adjacent à |2   |

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Les graphes : quelques éléments théoriques
{{< /pcache >}}
### 1.3. Des représentations
À propos de la lecture :

Grandjean, M. (2017). _Structures complexes et organisations internationales_. https://halshs.archives-ouvertes.fr/halshs-01610098v2

{{< pnotec >}}

L'entreprise intellectuelle de Martin Grandjean est "un état des lieux, une typologie et une réflexion sur les usages de l’analyse de réseau en histoire".
Il s'agit de faire des représentations graphiques ou symboliques, de sortir des mesure quantitatives pour analyser les relations _entre personnes_.
En effet "la façon dont des personnes et des institutions structurent leur organisation est une information qualitative de premier plan pour l’historien".

Martin Grandjean propose et présente 4 réseaux, autant de méthodes et d'outils pour la recherche en histoire en utilisant les graphes :

- le réseau métaphore : entremêlement puis circulation, puis "réseau social" ;
- le réseau reconstitué : une infographie, "une information traduite sous la forme d’une image ayant les caractéristiques visuelles d’un réseau", pas une visualisation de données ! Il peut y avoir des problèmes de représentativité, et d'hétérogénéité malgré une impression (graphique) d'homogénéité ;
- le réseau de contenu : "tirer le réseau directement du contenu des documents", c'est une autre manière de _voir_ les données. Martin Grandjean donne l'exemple des fiches de personnel, donc des données structurées qu'il peut manier et faire parler ;
- le réseau de métadonnées : "S’intéresser aux métadonnées d’un document, c’est considérer qu’il est possible de partir du principe que celui-ci, peu importe son contenu, est le témoignage d’une relation entre des individus." Ici il est nécessaire de grand corpus même si ça peut être possible sur de plus petits.

Ce que dit en creux Martin Grandjean : il faut expérimenter et jouer sur les données de visualisation.

Le but de tout cela ? "mettre au point une procédure sérielle sur un grand ensemble de pièces pour en tirer une information structurelle".

{{< /pnotec >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Gephi : un logiciel libre

{{< figure src="tux.png" legende="Tux, la figure animalière représentant le logiciel libre" >}}

{{< pnote >}}
Gephi est un logiciel libre, ce qui signifie que son utilisation, la lecture de son code source, sa modification et la redistribution de modifications sont autorisées et libres.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Gephi : un logiciel libre 
{{< /pcache >}}
### 2.1. Un logiciel de visualisation de données

- rendu graphique de relations entre des éléments
- traitements de données (y compris traitements algorithmiques)
- nombreux réglages
- exports possibles

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Gephi : un logiciel libre
{{< /pcache >}}
### 2.2. Logiciel libre ?


0. la liberté d'exécuter le programme, pour tous les usages ;
1. la liberté d'étudier le fonctionnement du programme et de l'adapter à ses besoins ;
2. la liberté de redistribuer des copies du programme (ce qui implique la possibilité aussi bien de donner que de vendre des copies) ;
3. la liberté d'améliorer le programme et de distribuer ces améliorations au public, pour en faire profiter toute la communauté.

{{< pnote >}}
Concrètement, cela signifie que l'usage de ce logiciel est gratuit, mais aussi que la mise à disposition de ce logiciel n'est pas faite dans l'optique de faire de l'argent via son accès.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Gephi : un logiciel libre
{{< /pcache >}}
### 2.3. Développement et maintien du logiciel

- code source disponible en ligne
- modalités de modification ouvertes
- gestion de projet : versionnement, tickets, discussions, wiki

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. Découverte et manipulations de Gephi

{{< figure src="gephi-2.png" legende="Capture d'écran du logiciel Gephi" >}}

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}

<!--
{{< psectioni >}}
{{< pcache >}}
## 1. 
{{< /pcache >}}
### 1.1. 

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. 
{{< /pcache >}}
### 1.1. 

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. 
{{< /pcache >}}
### 1.1. 

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}

-->
