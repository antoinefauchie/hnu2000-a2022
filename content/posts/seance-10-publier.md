---
title: "Séance 10 - Publier"
numero: "10"
date: 2022-08-29
date_p: 2022-11-17
lecture: "Blanchard, A. (2010). Ce que le blog apporte à la recherche. Dans M. Dacos (dir.), _Read/Write Book : Le livre inscriptible_ (p. 157‑166). OpenEdition Press. http://books.openedition.org/oep/172<br>Schrijver, E. (2017). Culture hacker et peur du WYSIWYG. _Back Office_, (1). http://www.revue-backoffice.com/numeros/01-faire-avec/eric-schrijver-culture-hacker-peur-wysiwyg"
layout: diapositive
visible: true
---
{{< psectioni >}}
{{< pcache >}}

## Plan

1. Que signifie publier sur le Web ?
2. Enjeux d'écriture : WYSIWYG
3. WordPress en pratique

{{< /pcache >}}
{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 1. Que signifie publier sur le Web ?

{{< figure src="tim-berners-lee-cern.jpeg" legende="Tim Berners-Lee au CERN en 1994" >}}

{{< pnote >}}
Pourquoi s'intéresser à la publication dans le cadre d'un cours sur les humanités numériques ?
Nous l'avons vu, les questions d'édition sont centrales et même fondatrices de l'approche des humanités numériques.
La publication n'est pas l'édition, mais elle en est une composante qu'il est important d'explorer dans un contexte où il est simple voir facile de rendre public des textes ou tout autre type de contenus.

Pour illustrer cela, voici Tim Berners-Lee, l'inventeur du Web (avec Robert Cailliau).
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}

{{< imageg src="robert-cailliau.png" >}}

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Que signifie publier sur le Web ?
{{< /pcache >}}
### 1.1. Publication VS édition

**Édition** :

- "Reproduction, publication et diffusion commerciale par un éditeur d'une œuvre sous forme d'un objet imprimé" (CNRTL)
- choix et production + légitimation + diffusion

**Publication** :

- "Action de rendre public, de faire connaître quelque chose à tous; résultat de cette action." (CNRTL)
- rendre public = production + diffusion
- pas de choix a priori, et pas de légitimation

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Que signifie publier sur le Web ?
{{< /pcache >}}
### 1.2. La (relative) liberté du Web

- concept initial du Web : lire et écrire
- marche technique importante
- web 2.0 : rendre accessible l'écriture
- le blog : un espace d'écriture personnel

{{< pnote >}}
Le Web s'est construit comme un espace de relative liberté dans un contexte où publier (donc rendre public) nécessitait justement un passage par de l'édition et surtout des instances éditoriales.
Le Web a permis, en théorie, de publier relativement facilement des contenus et de les diffuser auprès de personnes qui ont des accès.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}

## Aparté : le blog
Commentaires de l'article :

Blanchard, A. (2010). Ce que le blog apporte à la recherche. Dans M. Dacos (dir.), _Read/Write Book : Le livre inscriptible_ (p. 157‑166). OpenEdition Press. http://books.openedition.org/oep/172

{{< pnotec >}}
Ce chapitre traite de la question du blog dans un contexte particulier : la recherche scientifique, et plus particulièrement la recherche en sciences dites _dures_.
Antoine Blanchard explore 5 dimensions :

- la production de connaissances certifiées : ce sont des chercheurs qui présentent leurs travaux en cours ;
- l'expertise et la vulgarisation : la recherche _en train de se faire_ devient accessible à tous et à toutes car mieux indexée que les articles de revues souvent coincées dans des plateformes complexes (voir pas du tout disponible en ligne) ;
- la formation et les compétences incorporées : pouvoir attirer l'attention pour demander de l'aide, des compléments, bref une recherche collective ;
- biens collectifs : une certaine idée des communs, avec souvent des contenus sous licence ouverte, réutilisable et diffusable ailleurs ;
- avantage compétitif et innovation : montrer tout de suite les avancées d'un domaine, quitte à modifier en cours de route ce qui doit l'être, ou à dépublier des contenus problématiques.

Antoine Blanchard souligne à juste titre que le blog est aussi un moyen d'écrire souvent et d'être reconnu pour cela.
Tenir un blog scientifique c'est autant donner à voir sa recherche que de mettre à l'épreuve des hypothèses sans dépendre d'un circuit de validation long et fastidieux.

En conclusion le blog semble, dans le cadre d'une recherche scientifique, un outil particulièrement utile pour les chercheur·e·s, voir même incontournable d'une certaine manière.

{{< /pnotec >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Que signifie publier sur le Web ?
{{< /pcache >}}
### 1.3. Contraintes techniques

- structurer des contenus
- produire un format lisible par les navigateurs
- rendre ces contenus accessibles via le Web
- définir des modes d'accès (nom de domaine, syndication, etc.)

{{< pnote >}}
La perspective d'une publication sur le Web peut être considérée comme une série de contraintes, et elles sont d'abord techniques : comment rendre disponible un contenu sur l'application d'Internet qu'est le Web ?
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Enjeux d'écriture : WYSIWYG

{{< figure src="wysiwyg.png" legende="Capture d'écran d'une interface WYSIWYG" >}}

{{< pnote >}}
Le mode WYSIWYG est encore celui qui prévaut dans les applications/logiciels d'écriture et de publication.
Comme nous l'avons déjà vu, il est remis en cause par des approches orientées vers le balisage, qui permettraient a priori de mieux maîtriser les contenus en terme de structuration, d'interopérabilité et de pérennité.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Enjeux d'écriture : WYSIWYG
{{< /pcache >}}
### 2.1. Du code au WYSIWYG et au WYSIWYM

- aux premiers temps : HTML et rien d'autre
- puis des outils d'édition _à la Word_
- comprendre ce que l'on fait : le sens avant la mise en forme

{{< pnote >}}

Il faut bien prendre conscience que les premières pages web publiées et disponibles l'ont été en écrivant directement des pages HTML, sans passer par des outils intermédiaires (comme WordPress).

Aujourd'hui une nouvelle tendance se dessine, la DX pour _developer experience_, ou le fait que les personnes qui développent les applications d'écriture et d'édition ou qui participer à un projet éditorial sur les aspects notamment techniques, veulent aussi disposer d'un certain confort.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Enjeux d'écriture : WYSIWYG
{{< /pcache >}}
### 2.2. L'intérêt du WYSIWYG ?

- ce que je vois en écrivant/éditant est ce que j'obtiendrai en publiant
- le mensonge du WYSIWYG : pas de connaissances techniques requises
- des boutons plutôt que des commandes : efficace si bien pensé, un désastre cognitif si les moyens ne sont pas mis

{{< pnote >}}

Il serait naïf de penser que parce qu'il y a des boutons sur lesquels cliquer pour déclencher une action, alors cela ne nécessite pas d'apprentissage ou de connaissance particulière.
Les interfaces graphiques ont leur langage propre, au même titre que des logiciels basés sur le texte et la commande comme un terminal.
Il est question d'apprendre et de s'approprier ces langages, mais rares sont les applications qui suppriment toute friction dans leur utilisation.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## Aparté : "Culture hacker et peur du WYSIWYG"
Lecture commentée et échanges autour de :

Schrijver, E. (2017). Culture hacker et peur du WYSIWYG. _Back Office_, (1). http://www.revue-backoffice.com/numeros/01-faire-avec/eric-schrijver-culture-hacker-peur-wysiwyg

{{< pnotec >}}
Quelques notes de lecture sur l'article d'Eric Schrijver :

- il faut replacer, comme toujours, cet article dans son contexte : publié en 2016 mais écrit en 2014, ce texte met en lumière les critiques parfois élitistes du mode texte au détriment du mode WYSIWYG ;
- à travers ce texte Eric Schrijver tente de démonter les arguments en faveur du format texte et du terminal pour promouvoir un accès plus démocratique et populaire ;
- la culture hacker, soit des informaticiens (majoritairement des hommes), ne serait pas enclin à développer des interfaces graphiques faciles à utiliser, préférant le mode texte plus puissant et moins sujet à des erreurs de mise en forme (ou de mise en sémantique maladroite) ;
- Eric Schijver espère que les deux mondes se rencontrent, et que les WYSIWYG prennent acte du mode texte pour les personnes qui souhaitent avoir cet accès.

Il faut noter que l'article d'Eric Schrijver est peut-être un texte d'anticipation finalement assez réaliste.
WordPress développe en effet depuis 2018 une interface d'écriture qui mime le plus possible l'interface finale du site.
Le but est de pouvoir modifier les contenus dans le même contexte que dans celui où ils seront publiés.

Il est nécessaire de mentionner ici Gemini : un protocole alternatif au Web et un format de balisage pensés comme un dispositif qui permet de lire comme on écrit, en mode texte.

{{< /pnotec >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Enjeux d'écriture : WYSIWYG
{{< /pcache >}}
### 2.3. Éditer/publier en direct

- des interfaces d'écriture _sur_ les sites web
- retirer les frictions
- **des** modes d'édition

{{< pnote >}}
Ainsi ce sont de plus en plus d'interfaces d'écriture numérique qui viennent se greffer directement sur les sites web, afin de faciliter l'édition voir de supprimer la fameuse _interface d'édition_ ou le _backend_.
Plus besoin de passer par une interface complexe dans laquelle il faut par ailleurs retrouver les contenus, s'adapter à une autre interface graphique, et avoir une autre page pour _voir_ le résultat dans son contexte final.

Retirer les fictions est l'un des objectifs des interfaces d'édition _en direct_, mais est-ce une bonne chose ?
Ces modes WYSIWYG poussés à l'extrême posent la question de ce qu'est éditer et publier, et de la nécessité de disposer d'un environnement d'édition à part entière avec des outils adaptés.

Ce qui est, à mon sens, un point positif et un certain espoir, c'est que les modes se superposent.
Il est désormais souvent possible de passer d'un mode à l'autre, soit d'une interface WYSIWYG à une interface WYSIWYM qui est souvent du contenu HTML modifiable.
Ces modes d'édition peuvent communiquer entre eux, et cela peut être l'occasion de montrer que le texte qui s'écrit peut prendre plusieurs formes.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. WordPress en pratique

{{< figure src="wordpress-com.png" legende="Capture d'écran du site WordPress.com en 2007" >}}

{{< pnote >}}
Ce que l'on peut voir ici c'est que WordPress a été une forme de réseau social, comme LiveJournal quelques années plus tôt.
La dimension de syndication et de commentaires a permis de créer une ou des communautés.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. WordPress en pratique
{{< /pcache >}}
### 3.1. Création d'un site sur wordpress.com

- créer un compte sur wordpress.com
- créer un site web en choisissant un forfait gratuit (attention il y a un piège)
- renseigner un nom et une description de votre choix pour ce site web

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. WordPress en pratique
{{< /pcache >}}
### 3.2. Création, édition et publication

- créer 3 articles, par exemple en utilisant des contenus issus d'autres travaux ou en copiant des contenus sous licence ouverte (sans oublier les crédits)
- naviguer entre le mode édition et le tableau de bord
- organiser ces articles avec des catégories et des étiquettes
- choisir un autre thème (Administration → Apparence), quels sont les contraintes liées à ce changement ?

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. WordPress en pratique
{{< /pcache >}}
### 3.3. Éditer

- ajouter une autre personne présente dans la classe pour lui permettre d'éditer les contenus
- demander à cette personne de modifier des contenus existants
- explorer le mode versions lorsque vous êtes dans l'édition d'un article
- essayer de modifier un même article en même temps, que se passe-t-il ?

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. WordPress en pratique
{{< /pcache >}}
### 3.4. Bonus

- changer le statut d'un article : brouillon, accès limité, etc.
- créer plusieurs autres articles puis effectuer des modifications sur plusieurs articles en même temps (statut, tags, catégories, etc.)
- exporter votre site, qu'est-il possible de faire avec ce fichier ?

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


