---
title: "Séance 03 - Chercher"
numero: "03"
date: 2022-08-29
date_p: 2022-09-22
lecture: "Huma-Num (2021). _ISIDORE a 10 ans !_ Consulté à l’adresse https://humanum.hypotheses.org/6762"
layout: diapositive
visible: true
---
{{< psectioni >}}
{{< pcache >}}
## Plan

1. Une histoire d'Internet et du Web (origines, principes, infrastructures + whois/)
2. Les moteurs de recherche (annuaires, index et PageRank + quelques astuces de recherche avec exercices)
3. Isidore.science (projet de recherche, fonctionnalités, exercices)

{{< /pcache >}}
{{< psectiono >}}


{{< psectioni >}}
## 1. Une histoire d'Internet et du Web

> Ce ne sont pas tant les « nouvelles technologies » en général, mais le réseau en lui-même qui a bouleversé notre rapport à la connaissance.  
> Alain Mille, "D'Internet au web" dans _Pratiques de l'édition numérique_

{{< pnote >}}
Avant de découvrir rapidement l'histoire d'Internet et du web, il faut comprendre que c'est un bouleversement (plutôt qu'une révolution) pour l'humanité.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< figure src="world-brain.png" legende="Extrait du documentaire World Brain de Stéphane Degoutin et Gwenola Wagon." >}}

{{< pnote >}}
Voir la vidéo en ligne : [https://vimeo.com/381166153#t=80s](https://vimeo.com/381166153#t=80s).
Pourquoi cette vidéo ?
Pour prendre conscience de la dimension physique de l'infrastructure d'Internet qui permet la possibilité même de l'existence du Web.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Une histoire d'Internet et du Web
{{< /pcache >}}
### 1.1. Origines

- 1962-1968 : ARPAnet
- 1969-1978 : Internet
- années 1970 et 1980 : accès aux machines
- 1984 : un réseau fonctionnel

{{< pnote >}}
Pour ces quelques périodes mises en avant, je vous renvoie à l'excellent chapitre d'Alain Mille dans _Pratiques de l'édition numérique_.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Une histoire d'Internet et du Web
{{< /pcache >}}
### 1.2. Principes : TCP/IP
TCP/IP : TCP (Transmission Control Protocol) et IP (Internet Protocol).

Une pile de protocoles :

1. physique
2. liaison
3. réseau
4. transport
5. application

{{< pnote >}}
Plus de détails sur ces protocoles :

1. physique : les câbles (cuivre ou fibre), éventuellement radio ;
2. liaison : Ethernet ou Wireless Ethernet (Wifi), il s'agit de déterminer comment les paquets sont acheminés ;
3. réseau : c'est la partie IP qui permet d'acheminer des paquets en donnant des adresses à toutes les machines connectées sur un réseau ;
4. transport : c'est la partie TCP, pour transférer les informations découpées en paquets et reconstituées en vérifiant qu'il ne manque rien (c'est ce qui permet à Internet d'être un réseau fiable) ;
5. application : il s'agit du Web, mais aussi d'autres applications comme le partage de fichiers (FTP) ou le courriel (IMAP et SMTP).

Pour en savoir plus sur Internet et ses protocoles, voici une série de vidéos très complètes : https://iletaitunefoisinternet.fr/.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Une histoire d'Internet et du Web
{{< /pcache >}}

{{< exercice >}}
### Expérimentation
Découvrir la _route_ pour accéder à un serveur :

- utiliser traceroute en ligne de commande
- lancer la commande `traceroute umontreal.ca`
- analyser les résultats

{{< /exercice >}}

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Une histoire d'Internet et du Web
{{< /pcache >}}
### 1.3. Le Web

- Internet ≠ Web
- le Web = une application d'Internet
- un protocole (HTTP) et des langages (HTML/CSS/JavaScript)

{{< pnote >}}
Le Web est une application d'Internet permettant de publier et de consulter facilement des informations.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Une histoire d'Internet et du Web
{{< /pcache >}}

{{< exercice >}}
### Exercice

- comment lire une page web via un autre outil qu'un navigateur ?
- utiliser la commande `curl` dans le navigateur
- exemple 1 : `curl https://hnu2000.quaternum.net/seance-03-chercher/index.html`
- exemple 2 : `curl https://mahdi.blog/raw-permalinks-for-accessibility/`

{{< /exercice >}}

{{< pnote >}}
Il est intéressant de noter que l'accès à un document disponible via Internet et le Web peut se faire via différentes façons, et pas uniquement avec un navigateur web.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Les moteurs de recherche
> Au principe d’autorité qui a fait la force du PageRank, Google substitue de plus en plus un principe d’efficacité qui renvoie de manière toujours plus appropriée vers l’internaute les choix que l’algorithme a appris de ses comportements.  
> Dominique Cardon, Dans l'esprit du PageRank, [https://www.cairn.info/revue-reseaux-2013-1-page-63.htm](https://www.cairn.info/revue-reseaux-2013-1-page-63.htm)

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Les moteurs de recherche
{{< /pcache >}}
### 2.1. Le Web Au commencent était les annuaires

Une liste classée de sites web sous la forme d'une arborescence par catégories.

{{< pnote >}}
La démarche est sensiblement différente : il s’agit d’une navigation à travers une arborescence plutôt qu’une requête.
Avantages : navigation linéaire par thématiques, parcours dans une arborescence logique, aperçu potentiellement exhaustif de l’existant.
Inconvénients : parfois une seule entrée pour un résultat qui concerne plusieurs thématiques, recherche fastidieuse.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Les moteurs de recherche
{{< /pcache >}}
### 2.2. Constituer des index

Un moteur de recherche a deux fonctions :

- l'indexation de pages web et de documents ;
- la recherche dans ces pages web et ces documents indexés.

{{< pnote >}}
Il y a quatre évolutions majeures des moteurs de recherche ces dernières années :

- interface de recherche (UI) ;
- options de recherche (de plus en plus masquées) ;
- algorithmes (mécanisme du moteur) ;
- utilisation du Web sémantique (aller chercher des informations structurées de façon automatique) ;
- personnalisation (traçage).

Phénomène de simplification de la recherche sur Google :

- suppression des options de recherche : simplification de l'interface et des comportements de recherche ;
- autocomplétion : orienter la recherche de l'utilisateur ;
- pertinence des résultats (et surtout des premiers résultats) ;
- "web sémantique" : afficher dans les résultats des données liées ;
- globalement : l'information va vers l'utilisateur.

Fonctionnement d'un moteur de recherche

1. des robots parcourent le Web, de liens en liens ;
2. les données enregistrées sont classées ;
3. un utilisateur effectue une requête de recherche ;
4. en fonction d'algorithmes le moteur de recherche propose des réponses ;
5. en fonction des requêtes le moteur de recherche peut _orienter_ ses robots (fréquence, précision).

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Les moteurs de recherche
{{< /pcache >}}
### 2.3. Le PageRank

- système de mesure quantitative de popularité d'une page web
- inspiré par la mesure des articles académiques (Science Citation Index)

{{< pnote >}}
Le PageRank mesure _quantitativement_ la popularité d'une page web, il fait partie d'autres indicateurs qui permettent le classement des pages web.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}

{{< figure src="pagerank.png" legende="Schéma du PageRank" >}}

{{< pnote >}}
Le système du PageRank, conçu et utilisé par Google, est relativement complexe.
Ce que l'on peut retenir c'est qu'il s'agit d'un système inspiré des publications académiques.
Le fonctionnement est le suivant : la popularité d'une page web A est mesurée en fonction du nombre de pages web qui pointent vers cette page web A.
Si les pages web qui pointent vers la page web A, alors la popularité sera d'autant plus importante.
Il s'agit donc de la prise en compte du nombre de pages web qui pointent vers la page, mais également indirectement du nombre de pages web qui pointent vers les pages web qui pointent vers la page web A.
(_mes excuses pour ces répétitions_)

Pour en savoir plus sur le fonctionnement du PageRank, voir la balado Le code a changé (à partir de 21 minutes : https://www.radiofrance.fr/franceinter/podcasts/le-code-a-change/le-francais-qui-a-vu-naitre-google-4402005) et "Dans l’esprit du PageRank", un article de Dominique Cardon dans la revue Réseaux (https://www.cairn.info/revue-reseaux-2013-1-page-63.htm).

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Les moteurs de recherche
{{< /pcache >}}

{{< exercice >}}
### Exercice
Dans Google ou DuckDuckGo :

- à partir des fonctions avancées suivantes :
  - chercher une expression exacte : `"mon expression exacte à la lettre près"`
  - chercher les contenus d'un site : `site:lesite.com ma requête`
	- chercher un format de fichier spécifique : `filetype:pdf ma requête`
	- chercher dans le titre d'une page web : `intitle:requête`
- chercher les pages web qui parle d'humanités numériques et d'édition sur les différents sites web de l'Université de Montréal

{{< /exercice >}}

{{< psectiono >}}


{{< psectioni >}}
## 3. Isidore.science
> ISIDORE est un moteur de recherche permettant de découvrir et de trouver des publications, des données numériques et profils de chercheur·e·s en sciences humaines et sociales (SHS) venant du monde entier.

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Isidore.science
{{< /pcache >}}
### 3.1. Un projet de recherche

- naissance en 2011
- objectifs : mettre à disposition un outil de recherche adapté aux sciences humaines
- ambition : proposer des fonctionnalités pour faciliter la recherche documentaire

{{< pnote >}}
> Initialement, ISIDORE est un moteur de recherche permettant de découvrir et de trouver des publications, des données numériques et profils de chercheur·e·s en sciences humaines et sociales (SHS) venant du monde entier.
>
> Il permet de rechercher dans le texte intégral de plusieurs millions de documents (articles, thèses et mémoires, rapports, jeux de données, pages Web, notices de bases de données, description de fonds d’archives, etc.) des signalements d’événements (séminaires, colloques, etc.). De plus, ISIDORE relie entre eux ces millions de documents en les enrichissant à l’aide de concepts scientifiques issus des travaux des communautés de recherche des SHS.

Typiquement Isidore ne base pas ses algorithmes de recherche sur un système type PageRank mais sur des référentiels permettant de classer et de trouver des documents selon des techniques documentaires.

Isidore se démarque donc à la fois des moteurs de recherche dits généralistes (comme Google), mais aussi d'autres outils de recherche ou de collecte comme Google Scholar (qui propose moins de fonctionnalités et dont le classement documentaire est assez flou ou inexistant), de plateformes spécialisées comme Cairn.info ou OpenEdition (bien moins exhaustifs), ou encore de Zotero (un outil de collecte et de partage, complémentaire d'Isidore).
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Isidore.science
{{< /pcache >}}
### 3.2. Fonctionnement/fonctionnalités

- moissonner
- construire des index
- enrichir des données
- proposer une API
- inclure un espace de travail

{{< pnote >}}
Isidore construit son index en allant moissonner des entrepôts de données, ces données sont enrichies à partir de référentiels et de thésaurus.
Les documents indexés peuvent être recherchés via l'interface de recherche, mais une API est aussi disponible pour que d'autres sites web ou des applications puissent utiliser ces données (principalement les interroger).
Isidore n'est pas qu'un moteur de recherche, c'est aussi un espace de travail proposant plusieurs fonctionnalités :

- accéder à son propre historique ;
- enregistrer des requêtes de recherche ;
- créer des collections de documents ;
- suivre un auteur ou une autrice : être informé des nouvelles parutions ;
- gérer ses alertes (auteurs·trices suivi·e·s, requêtes, etc.).

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Isidore.science
{{< /pcache >}}

{{< exercice >}}
### Exercices

- constituez une bibliographie dans Isidore.science sur les humanités numériques et l'édition
- pour cela vous devez vous créer un compte Isidore (et donc HumanID)
- utilisez les fonctionnalités de la recherche avancée (un peu cachée : https://isidore.science/as) ;
- utilisez les fonctions de tris et de facettes ;
- partagez avec moi votre "bibliothèque".

{{< /exercice >}}

{{< pnote >}}
À partir de cette expérience, que manque-t-il à Isidore ?
Quels sont ses défauts ?
{{< /pnote >}}
{{< psectiono >}}

