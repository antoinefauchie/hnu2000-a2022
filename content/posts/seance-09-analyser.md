---
title: "Séance 09 - Analyser"
numero: "09"
date: 2022-08-29
date_p: 2022-11-10
lecture: "Sinclair, S. et Rockwell, G. (2020). _E-texts and Analysis: Teaching Guide_ [documentation]. dialogica. https://voyanttools.github.io/dialogica/etexts.html"
layout: diapositive
visible: false
---

{{< psectioni >}}

{{< pnote >}}
Quelques notes sur le chapitre "Les potentialités du texte numérique" :

- l'informatique comme moyen d'obtenir plus et de meilleurs résultats ;
- "le support numérique traite déjà chaque lettre comme une entité indépendante et mobile" : le numérique change les choses ;
- 
{{< /pnote >}}
{{< psectiono >}}

Un ordinateur ne sait pas gérer du texte, des chiffres oui, mais le texte est trop complexe à gérer pour un ordinateur pour l'instant, sans parler du _sens_ des textes.
Donc il faut trouver des moyens de traiter les textes comme des données, par exemple en recherchant des _modèles_ de textes : `travail*` pour chercher "travail", "travailler", "travailleur" ou "travailleuse".

Voyant Tools permet

Si le texte n'a aucune sémantique il va falloir travailler le texte pour retirer les mots inutiles ou vides, dans notre cas.
Ces _stop words_ sont notamment

Attention il s'agit d'un outil statistique.

Plusieurs approches sont possibles :

- exploratoire : pour découvrir quelques patterns ou simplement connaître 
- dans un but précis : par exemple en cherchant une notion spécifique.

Yann nous a montré la fonction Mandala : pour visualiser des groupes de termes par rapport aux éléments d'un corpus.

Bubbles : pouvoir 


- voir le texte
- découvrir des inconsistances (avec des textes qu'on connaît bien)
- défamiliarisation des textes : découvrir un texte autrement que par une lecture linéaire
- voir des tendances

Chercher ou mieux identifier ce qu'on connaît déjà.


