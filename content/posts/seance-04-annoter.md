---
title: "Séance 04 - Annoter"
numero: "04"
date: 2022-08-29
date_p: 2022-09-29
lecture: "Fredriksson, S. & Sauret, N. (2019). Écrire les communs. Au-devant de l’irréversible. _Sens public_. Consulté à l’adresse http://sens-public.org/dossiers/1383/"
layout: "diapositive"
hypothesis: true
visible: true
---
{{< psectioni >}}
{{< pcache >}}

## Plan

1. Pourquoi annoter ?
2. Annoter le Web : une question de standards
3. Hypothesis en pratique

{{< /pcache >}}
{{< psectiono >}}


{{< psectioni >}}
## 1. Pourquoi annoter ?

> […] en quoi l’écrit transforme et diversifie le répertoire d’action des communs, leur définition-même, et l’identité de ceux et celles qui y prennent part […] ?  
> […]  
> Ce dossier, en édition continue, se veut un appel à l’écriture comme une invitation au mouvement. Une invitation à construire un espace public où s’opèrent des rencontres singulières, des croisements inattendus.  
> Écrire les communs. Au-devant de l’irréversible, Sylvia Fredriksson et Nicolas Sauret

{{< pnotec >}}
C'est une réflexion à la fois ambitieuse et enrichissante.

Voici plusieurs points saillants qui nous invitent à considérer les annotations selon un statut particulier, comme un acte d'écriture à part entière :

- les communs : "Les communs sont des ressources partagées, gérées et maintenues collectivement par une communauté ; celle-ci établit des règles dans le but de préserver et pérenniser ces ressources 1 tout en fournissant aux membres de cette communauté la possibilité et le droit de les utiliser, voire, si la communauté le décide, en octroyant ce droit à tous. Ces ressources peuvent être naturelles (une forêt, une rivière), matérielles (une machine-outil, une maison, une centrale électrique) ou immatérielles (une connaissance, un logiciel)." ([Source](https://fr.wikipedia.org/wiki/Communs)) ;
- avec les communs viennent des pratiques diverses d'écriture et de publication, pratiques souvent collectives ;
- décrire les dynamiques c'est les faire exister !
- il est nécessaire de mettre en place des dispositifs de circulation de la connaissance, et donc d'imaginer des licences plus ouvertes : Open Access, Creative Commons ou les systèmes de validation de Wikipédia (par ailleurs criticables) ;
- du hack à la reconfiguration : prototyper pour mieux faire des changements de fond. Avec l'idée de l'**itération** ;
- "La vision que défend Sens Public depuis sa création s’inscrit pleinement dans l’idée que l’espace public se constitue dans le geste de publier."

Mais alors pourquoi parler de ce texte dans le cadre d'un cours sur l'annotation ?…
Pour au moins trois raisons :

1. annoter un texte c'est lire activement
2. annoter un texte c'est écrire
3. annoter un texte c'est converser avec l'auteur·trice

L'annotation est donc une pratique qui permet d'enrichir sa littératie numérique, et de porter un regard critique sur nos pratiques et nos outils de lecture et d'écriture.

{{< /pnotec >}}
{{< psectiono >}}


{{< psectioni >}}
{{< figure src="bayle.jpg" legende="Extrait du Dictionnaire historique et critique de Pierre Bayle réédité par les Belles lettres" >}}

{{< pnote >}}
Le _Dictionnaire historique et critique_ de Pierre Bayle est un très bon exemple de glose.
Dans cet ouvrage Pierre Bayle engage une conversation avec son propre texte à travers de nombreux paratextes et des liens _dans_ le livre.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Pourquoi annoter ?
{{< /pcache >}}
### 1.1. Qu'est-ce qu'une annotation ?

- glose ?
- paratexte ?
- commentaire ?

{{< pnote >}}
Avant de rentrer dans les détails d'une probable définition, retenons que l'annotation est le résultat d'un double mouvement de lecture et d'écriture : lorsque je lis un texte j'écris sur/à côté/dans le texte.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< figure src="glose.jpg" legende="De plantis glosé (ms. British Library, Harley 3487)" >}}

{{< pnote >}}
La glose est un commentaire ou une annotation dans les marges ou entre les lignes d'un texte.
Il s'agit le plus souvent d'un échange unilatéral entre deux auteurs différents.

Le paratexte est l'ensemble des éléments entourant le texte sans en faire partie : des notes, une préface, mais aussi des métadonnées comme le titre. 
L'annotation peut être considéré comme un paratexte en cela qu'il vient compléter le texte, lui donner un autre élan.
(Voir Gérard Génette pour plus de compléments sur ce concept.)

Enfin le commentaire agit plus comme un complément, une explication.
Une définition plus actuelle concerne les textes écrits par des personnes sur des pages web _en réaction_.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Pourquoi annoter ?
{{< /pcache >}}
### 1.2. Le Web, un espace de lecture et d'écriture

- aux origines : consulter autant qu'héberger
- dans les faits : consommation

{{< pnote >}}
Internet et le Web est un projet presque utopique :

- le Web a été pensé comme un espace d'écriture et de publication accessible : chacun·e pouvant héberger son propre site dans son garage ;
- dans les faits le Web est devenu majoritairement un espace de lecture (de consommation).

Les protocoles d'Internet et du Web sont des protocoles de lecture et d'écriture, par exemple en HTTP avec GET et POST :

- GET : méthode pour _demander_ une ressource, il s'agit d'une forme de lecture ;
- POST : pour _transmettre_ des informations, il s'agit d'une forme d'écriture.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Pourquoi annoter ?
{{< /pcache >}}
### 1.3. En pratique : commenter des textes

- annoter quoi ?
- le Web : des documents disponibles **et structurés**

{{< pnote >}}
La question de l'annotation dans l'espace numérique pose des questions nouvelles par rapport aux annotations dans l'espace analogique du papier.
Nous pouvons toutes et tous nous accorder sur le fait qu'annoter un livre imprimé est très différent de l'annotation d'une page web :

- le numérique ne limite pas en _place_ : l'annotation peut être encore plus longue que le texte annoté lui-même ;
- le numérique ne limite pas en copie : une annotation peut être visible par bien de personnes qu'avec un exemplaire imprimé qui sera plus compliqué à copier ;
- le numérique complique les enjeux d'archivage.

Nous devons nous poser les questions suivantes :

- quels sont les formats ou les dispositifs de lecture de ces textes ?
- comment trouver un moyen efficient pour annoter des documents textuels ?

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Annoter le Web : une question de standards


{{< pnote >}}
Pourquoi parler de standards ?
Parce que pour annoter quelque chose, il faut que ce quelque chose soit défini, quelles sont ses spécificités, que l'on sache comment il se comporte.

- qu'est-ce qu'un standard ?
- qu'est-ce qu'un format ?
- un standard parmi d'autres : HTML

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< figure src="whatwg.png" legende="Capture d'écran de la page d'accueil du Web Hypertext Application Technology Working Group (WHATWG)" >}}

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Annoter le Web : une question de standards
{{< /pcache >}}
### 2.1. Qu'est-ce qu'un standard ?
> Modèle de référence, norme adoptée par l'usage, par un groupe de personnes.  
> CNRTL, [https://www.cnrtl.fr/definition/standard](https://www.cnrtl.fr/definition/standard)

{{< pnote >}}
Un standard n'est pas un protocole (protocole = règles nécessaires à l'établissement d'une communication), pas plus qu'une norme (norme = règle définie, condition que doit respecter une réalisation).

Dans notre cas nous abordons la double question du standard et du format, puisque nous nous intéressons à la façon dont un contenu est structuré afin d'y _accrocher_ une annotation.
L'enjeu d'annoter un document sur le Web est celui de savoir où est ce document et comment l'information est structurée.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Annoter le Web : une question de standards
{{< /pcache >}}
### 2.2. Qu'est-ce qu'un format ?

{{< image src="a-size.png" >}}

{{< pnote >}}
Pour parler de format le plus simple est de regarder comment cela fonctionne dans le monde physique, ici par exemple les _formats_ de papier.
Le format est donc une suite d'instructions, le but est de constituer une série d’informations compréhensible, utilisable et communicable.

Un format informatique est une spécification technique, il structure des informations, il est le pivot entre une organisation logique et son implémentation dans un système informatique.
Un fichier doit avoir un format, sans quoi il ne pourra être produit, transmis ou lu.
Un format informatique est le lien entre l’infrastructure et la personne qui utilise cette infrastructure.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Annoter le Web : une question de standards
{{< /pcache >}}
### 2.3. Un standard parmi d'autres : HTML

- précurseur : SGML
- une série d'éléments et des règles d'utilisation
- baliser du texte (et des interactions _autour_ de ce texte)

{{< pnote >}}
Le format ou langage HTML est un standard puisqu'il est :

- défini par un ensemble de personnes qui forment une communauté ;
- décrit dans une série de documents permettant de comprendre son fonctionnement et ses contraintes ;
- adopté en pratique par un ensemble d'acteurs et de personnes ;
- diffusé à travers des recommandations précises.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. Hypothesis en pratique


{{< pnote >}}

- mini-étude de cas
- exercice 1 : annoter, commenter
- exercice 2 : les groupes
- exercice 3 : chercher dans les annotations, jouer avec l'API

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< figure src="" legende="" >}}

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Hypothesis en pratique
{{< /pcache >}}
### 3.1. Hypothesis, un outil d'annotation pour le Web
> Annotate the web, with anyone, anywhere.  
> [hypothes.is](https://hypothes.is)

Hypothesis est un outil d'annotation, plus spécifiquement un service en ligne permettant d'annoter des pages web avec des fonctions de discussion et de partage avancés.

{{< pnote >}}
Hypothesis a été créé en 2011 par Dan Whaley, un entrepreneur et dirigeant d'entreprise.
L'idée initiale d'Hypothesis était simple : trouver le moyen d'annoter des pages web et de partager ces annotations.
Ce projet a été rendu possible par la création d'un groupe de travail au sein du W3C [https://www.w3.org/annotation/](https://www.w3.org/annotation/), qui a donné lieu à un certain nombre de recommandations ([Web Annotation Data Model](http://www.w3.org/TR/annotation-model/), [Web Annotation Vocabulary](http://www.w3.org/TR/annotation-vocab/) ou [Web Annotation Protocol](http://www.w3.org/TR/annotation-protocol/)) qui permettent de modéliser les annotations dans des formats et des structurations basées sur des standards en vue de la réutilisation de ces annotations.

Hypothesis est une organisation à but non lucratif, elle est à l'origine d'une autre organisation, anno., une société d'intérêt public créée pour pouvoir assurer une activité économique à Hypothesis.

Il faut noter que lorsque Hypothesis est lancé, les applications d'annotation sont rares (notamment pour des questions d'absence de modèle ou de standard), ou en tout cas elles ne proposent pas un modèle aussi ouvert avec cette dimension standardisation et interopérabilité.
À noter également que Hypothesis est d'abord financé par la communauté avec une campagne de financement participatif sur Kickstarter.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< figure src="hi-f.png" legende="Capture d'écran de la présentation du fonctionnement d'Hypothesis" >}}

{{< pnote >}}
Concrètement Hypothesis permet d'annoter des pages web via un outil directement accessible en ligne (application et service web).
Pour cela il faut tout d'abord disposer d'un compte, et ensuite annoter une page web selon deux modes :

- lorsque Hypothesis est déjà _installé_ sur un site web ;
- en utilisant le [Bookmarklet](https://web.hypothes.is/start/) ;
- en utilisant l'extension pour [Chrome](https://web.hypothes.is/help/installing-the-chrome-extension/) ou [Firefox](https://addons.mozilla.org/en-US/firefox/addon/hypothes-is-bookmarklet/).

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< figure src="hi-e.png" legende="Capture d'écran d'une page web annotée" >}}

{{< pnote >}}
Hypothesis permet d'annoter, de surligner ou de créer une note.
Voici un détail de ces actions :

- annoter : à partir d'un fragment de texte sélectionné, il est possible d'ajouter un texte qui sera enregistré dans la base de données d'Hypothesis. Ce texte est attaché à la page web et au passage précis ;
- surligner : il est possible de simplement mettre en emphase un passage sans associer de commentaire/annotation ;
- note : il s'agit d'un texte qui accompagne la page web, sans lien précis avec un passage _dans_ la page web.

Voici ci-dessous un exemple d'export d'une annotation au format JSON : il s'agit d'un format structurant les données du site web où a été faite l'annotation ainsi que 
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
```
[
  {
    "id": "sHg40oufEeqJ2kvVr8VdOw",
    "authority": "hypothes.is",
    "url": "https://www.quaternum.net/2020/04/30/fabriques-de-publication-pandoc/",
    "created": "2020-05-01T11:34:16",
    "updated": "2020-05-01T11:34:16",
    "title": "Fabriques de publication : Pandoc",
    "refs": [],
    "isReply": false,
    "isPagenote": false,
    "user": "loupbrun",
    "displayName": "Louis-O Brassard",
    "text": "Comme ici: https://arthurperret.fr/2018/12/04/semantique-et-mise-en-forme/",
    "prefix": "Pandoc est souvent qualifié de “",
    "exact": "couteau suisse de l’édition",
    "suffix": "”.\n\nDescription\nPandoc est un lo",
    "start": 762,
    "end": 789,
    "tags": [],
    "group": "__world__",
    "target": [
      {
        "source": "https://www.quaternum.net/2020/04/30/fabriques-de-publication-pandoc/",
        "selector": [
          {
            "type": "RangeSelector",
            "endOffset": 372,
            "startOffset": 345,
            "endContainer": "/div[1]/main[1]/article[1]/p[3]",
            "startContainer": "/div[1]/main[1]/article[1]/p[3]"
          },
          {
            "end": 789,
            "type": "TextPositionSelector",
            "start": 762
          },
          {
            "type": "TextQuoteSelector",
            "exact": "couteau suisse de l’édition",
            "prefix": "Pandoc est souvent qualifié de “",
            "suffix": "”.\n\nDescription\nPandoc est un lo"
          }
        ]
      }
    ],
    "document": {
      "title": [
        "Fabriques de publication : Pandoc"
      ]
    }
  }
]
```

{{< pnote >}}
Il est difficile de comparer Hypothesis avec d'autres services de nature similaire, puisque les applications d'annotation se limitent souvent à quelques usages très spécifiques :

- l'annotation de PDF dans pléthores d'applications ou logiciels, y compris Zotero ;
- l'annotation de livres ou d'autres documents dans des applications de lecture numérique ;
- des pratiques proches de l'annotation mais qui ne proposent pas une précision aussi importante.

Le fonctionnement adopté par Hypothesis permet de conserver une annotation et sa localisation même si la page web a changé (c'est-à-dire sa structure HTML).
C'est toute la force de Hypothesis de proposer un algorithme puissant capable de garder l'emplacement d'une annotation.

Hypothesis est à la fois une démarche, une application, un service et une communauté.
Il ne faut pas réduire Hypothesis à un outil d'annotation, il y a derrière cet outil une initiative qui vise à mettre en place un standard, ainsi que d'imaginer des usages autour des pratiques d'annotation (d'abord en milieu académique).
Cette dualité (mettre en place un service et réaliser les standards qui vont avec) est

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Hypothesis en pratique
{{< /pcache >}}
### Exercice 1 : annoter 

- créez-vous un compte Hypothesis
- affichez la page de la séance
- connectez-vous à Hypothesis
- **annotez cette page en indiquant quelques questions**
- rendez-vous sur la page de votre profil Hypothesis

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Hypothesis en pratique
{{< /pcache >}}
### Exercice 2 : partager

- rejoignez le groupe privé dédié au cours : [hypothes.is/groups/LwdMDKBM/hnu2000-a2022](https://hypothes.is/groups/LwdMDKBM/hnu2000-a2022)
- modifiez vos annotations pour qu'elles fassent partie du groupe privé
- ajoutez des _tags_ à vos annotations
- répondez à quelques annotations déjà existantes

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Hypothesis en pratique
{{< /pcache >}}
### Exercice 3 : rechercher

- sur la page [https://hypothes.is/](https://hypothes.is/) vous pouvez rechercher des annotations
- effectuez quelques recherches avec l'expression `url:https://adresse.com/*` avec les adresses de votre choix
- sur la page [https://jonudell.info/h/facet/](https://jonudell.info/h/facet/) effectuez à nouveau ces recherches et exportez-les en JSON

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}

