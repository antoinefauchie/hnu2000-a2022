---
title: "Séance 02 - 10 outils des humanités numériques"
numero: "02"
date: 2022-08-29
date_p: 2022-09-15
lecture: "Lorusso, S. (2021). Liquider l’utilisateur. _Tèque(1)_. 10–57."
bibfile: data/bibliographie.json
visible: true
layout: diapositive
---
{{< psectioni >}}
## Plan de la séance

1. Vous avez dit outils ?
2. Outils et changement(s)
3. 10 outils
4. Qu'est-ce qu'une étude de cas ?

{{< psectiono >}}


{{< psectioni >}}
## 1. Vous avez dit outils ?
> Objet fabriqué, utilisé manuellement, doté d'une forme et de propriétés physiques adaptées à un procès de production déterminé et permettant de transformer l'objet de travail selon un but fixé.
>
> Moyen; ce qui permet d'obtenir un résultat, d'agir sur quelque chose.
>
> CNRTL, [https://www.cnrtl.fr/definition/outils](https://www.cnrtl.fr/definition/outils)

{{< pnote >}}
Citer Simondon.
Question de l'artefact ?
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Vous avez dit outils ?
{{< /pcache >}}

- logiciel
- programme
- application
- service

{{< pnote >}}
Un logiciel est un ensemble de programme permettant de traiter des informations, souvent par le biais d'une interface homme-machine ou _graphique_.
Contrairement au _programme_, le logiciel est utilisable tel quel afin de réaliser une ou des opérations précises.
Exemples de logiciels : Microsoft Word, Zotero ou le navigateur web Firefox.

Un programme ou programme informatique est un ensemble d'instructions et d'opérations que réalise un ordinateur, il s'agit le plus souvent d'une partie d'un logiciel ou d'une application.
On peut considérer un programme comme une brique à l'intérieur d'un système qui réalise des opérations complexes.
Exemples de programmes informatiques : le convertisseur Pandoc, un script Python permettant d'automatiser des tâches de renommage de fichiers ou un validateur de format.

Une application est un logiciel, souvent proposée sous une forme plus spécifique comme pour certains systèmes d'exploitation ou directement en ligne.
Ici nous parlons d'application lorsque celle-ci propose un ensemble de services en plus d'exécuter des tâches précises.
De ce fait, les applications sont souvent des logiciels disponibles sur le Web ou connectés à d'autres logiciels disponibles en ligne.
Exemples d'applications : Stylo, Zotero en ligne ou Spotify.

Un service correspond à une ou plusieurs tâches réalisées par un ordinateur, souvent disponibles en ligne sur le Web.
Un service se distingue d'une application ou d'un logiciel en proposant un hébergement, une fonctionnalité liée à des données tierces ou encore 
Un service peut être compris dans un logiciel ou une application.
Exemples de services : l'hébergement proposé par Zotero, l'API de Twitter ou encore un service de stockage et de partage de fichiers en ligne.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Vous avez dit outils ?
{{< /pcache >}}

Plongée dans le texte de Silvio Lorusso {{< cite  "lorusso_liquider_2021" >}}.

> Quelles sont les conditions pour que l'utilisateur·rice/usager·ère d'un ordinateur puisse échapper à la routinisation de son comportement ?  
> _Préface du texte_

{{< pnotec >}}
Le texte de Silvio Lorusso est dense, mais nous pouvons nous arrêter sur plusieurs éléments importants pour comprendre sa démarche et sa pensée :

- action : utiliser les outils/logiciels/applications informatiques requièrent une forme d'abandon, on fait ce que la machine nous indique de faire, quitte à se retrouver enfermer dans des usages. Plutôt qu'un "véhicule" l'ordinateur doit être un outil : l'humain doit être capable de faire des choix ;
- "Pour paraphraser Kay, les choses simples ne sont pas restées simples et les choses compliquées sont devenues moins possibles. (p. 28)"
- champ des possibles : l'informatique serait un champ des possibles, et cela n'est possible qu'en apprenant son usage, ce qui est devenu presque impossible avec des interfaces graphiques qui cachent le fonctionnement des machines ;
- littératie : deux visions opposées en apparence, avec d'un côté la facilité d'accès et de l'autre l'autonomie absolue. Lecture ou écriture ;
- boîte noire : en cachant les rouages on est enfermé ;
- linéarité et non-linéarité : suivre un protocole sans forcément suivre une liste stricte ;
- l'automatisation : au service d'une plus grande liberté de choix et non comme une industrialisation excessive ;
- rugosité : savoir appréhender des environnements complexes (mais pas forcément compliqués).

{{< /pnotec >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Outils et changement(s)
**Quel est l'outil\* qui a changé votre façon de travailler ces 15 dernières années ?**

\* Travailler au sens large = activité.

{{< psectiono >}}


{{< psectioni >}}
## 3. 10 outils
### 3.1. Isidore.science
> Votre assistant de recherche en Sciences humaines et sociales

{{< image src="isidore.png" >}}

{{< pnote >}}
Isidore.science est un moteur de recherche pour les sciences humaines et sociales permettant d'interroger des bases de données d'articles, d'ouvrages et autres ressources.
En plus de permettre une recherche adéquate (avec des options de recherche pertinentes), Isidore.science propose des fonctionnalités pour _gérer_ les résultats.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
### 3.1. Isidore.science
{{< /pcache >}}

- fonction(s) : moteur de recherche, gestion de résultats
- usage(s) : recherche de ressources scientifiques, enregistrement de données
- création : Huma-Num, 2011
- mise à jour : très régulières
- changement(s) induit(s) : des pratiques de recherche adaptées, non dictées par un acteur commercial

{{< pnote >}}
Pour comprendre ces enjeux, il est nécessaire de découvrir le fonctionnement sous-jacent d'Internet et du Web.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
{{< /pcache >}}
### 3.2. Hypothes.is
> Annotate the web, with anyone, anywhere.

{{< image src="hypothesis.png" >}}

{{< pnote >}}
Hypothes.is est un outil d'annotation pour le Web, il est basé sur les standards du Web et permet de créer des conversations.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
### 3.2. Hypothes.is
{{< /pcache >}}

- fonction(s) : annoter des pages web
- usage(s) : créer des conversations sur des documents
- création : 2011, Dan Whaley, structure mixte
- mise à jour : développements continus
- changement(s) induit(s) : implémentation d'un standard pour annoter tout document numérique

{{< pnote >}}
Pour bien appréhender Hypothes.is il faut comprendre le fonctionnement du Web en détail ainsi que les enjeux des standards.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
{{< /pcache >}}
### 3.3. Stylo
> Stylo est un éditeur de texte sémantique pour les sciences humaines.

{{< image src="stylo.png" >}}

{{< pnote >}}
Stylo est un outil conçu pour repenser le processus d'écriture et d'édition des revues savantes en sciences humaines et sociales. En tant qu'éditeur de texte sémantique WYSIWYM (What You See Is What You Mean, Ce que vous voyez est ce que vous signifiez) pour les sciences humaines, il vise à améliorer la chaîne de publication académique.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
### 3.3. Stylo
{{< /pcache >}}

- fonction(s) : écrire et structurer du texte dans une interface web
- usage(s) : créer des articles scientifiques en respectant des protocoles d'édition
- création : 2008, Chaire de recherche du Canada sur les écritures numériques
- mise à jour : plusieurs fois par an
- changement(s) induit(s) : repenser l'écriture scientifique et les chaînes de publication académique

{{< pnote >}}
Stylo est basé sur des standards et des protocoles, c'est aussi un outil qui fonctionne grâce au balisage sémantique et grâce à d'autres outils libres.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
{{< /pcache >}}
### 3.4. Zotero
> Your personal research assistant

{{< image src="zotero.png" >}}

{{< pnote >}}
Zotero est un logiciel permettant de récolter et d'organiser des références bibliographiques, mais aussi un service en ligne permettant de sauvegarder et de partager des références bibliographiques.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
### 3.4. Zotero
{{< /pcache >}}

- fonction(s) : enregistrer, organiser, sauvegarder et partager des références bibliographiques
- usage(s) : création et gestion de références bibliographiques à usage universitaire et éditorial
- création : 2006, Roy Rosenzweig Center for History and New Media
- mise à jour : évolutions très régulières
- changement(s) induit(s) : outil de gestion de références bibliographiques libre et gratuit

{{< pnote >}}
Zotero est un outil qui semble parfois trop avancé pour une question si simple que la création de références bibliographiques ou de bibliographies.
Dans les faits Zotero se trouve être un outil incontournable, mais pour arriver à cette conclusion il faut appréhender les questions de contraintes éditoriales en milieu académique, ainsi que la définition d'une bibliographie structurée (ou de toute donnée structurée en général).
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
{{< /pcache >}}
### 3.5. Ngram Viewer
> Ngram Viewer est une application linguistique proposée par Google, permettant d’observer l’évolution de la fréquence d’un ou de plusieurs mots ou groupes de mots à travers le temps dans les sources imprimées.  
> Source : [Wikipédia](https://fr.wikipedia.org/wiki/Ngram_Viewer)

{{< image src="ngram-viewer.png" >}}

{{< pnote >}}
Ngram Viewer est un pur produit de Google : il s'agit d'interroger les livres numérisés par Google autant que d'utiliser des algorithmes de recherche produits par Google.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
### 3.5. Ngram Viewer
{{< /pcache >}}

- fonction(s) : recherche, comparaison et affichage de termes dans un corpus défini
- usage(s) : comparaison de termes dans l'un des plus importants corpus de livres numérisés
- création : 2010, Google
- mise à jour : février 2020 (pas possible d'interroger les livres publiés après 2019)
- changement(s) induit(s) : comparer _facilement_ un immense corpus, avec une visualisation graphique

{{< pnote >}}
Il faut découvrir comment un livre est numérisé pour prendre la mesure d'une telle application, ainsi que comprendre comment un moteur de recherche permet de _parser_ une quantité de données aussi importante.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
{{< /pcache >}}
### 3.6. Voyant Tools
> Voyant Tools est un environnement en ligne de lecture et d’analyse de textes numériques.

{{< image src="voyant-tools.png" >}}

{{< pnote >}}
Voyant Tools est un projet universitaire proposant un environnement de lecture et d'analyse de texte sur le Web.
C'est un outil pédagogique permettant de comprendre ce qu'est la fouille de texte.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
### 3.6. Voyant Tools
{{< /pcache >}}

- fonction(s) : extraction, calcul de la fréquence, production de statistiques, etc.
- usage(s) : outil d'analyse de texte
- création : 2003, Stéfan Sinclair et Geoffrey Rockwell
- mise à jour : développements réguliers
- changement(s) induit(s) : disposer d'outils d'analyse de texte accessibles

{{< pnote >}}
La question du logiciel libre est ici importante : comment un programme informatique ou une application web peut être créée et maintenue ?
Cette application permet de comprendre les enjeux (complexes) de la fouille de texte.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
{{< /pcache >}}
### 3.7. WordPress
> Create a place for your business, your interests, or anything else—with the open source platform that powers the web.

{{< image src="wordpress.png" >}}

{{< pnote >}}
WordPress est le système de gestion de contenus sur le Web le plus populaire et le plus répandu.
C'est un CMS basé sur le principe du WYSIWYG.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
### 3.7. WordPress
{{< /pcache >}}

- fonction(s) : créer et gérer des sites web (pas uniquement des blogs)
- usage(s) : création et gestion de sites web
- création : 2003, Matthew Mullenweg et Mike Little
- mise à jour : une ou plusieurs fois par an
- changement(s) induit(s) : écrire et publier sur le Web sans connaissances techniques

{{< pnote >}}
WordPress est l'exemple typique d'une interface WYSIWYG directement héritée des traitements de texte, mais aussi une application qui repose sur les technologies du Web.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
{{< /pcache >}}
### 3.8. Gephi
> Gephi is the leading visualization and exploration software for all kinds of graphs and networks. Gephi is open-source and free.

{{< image src="gephi.png" >}}

{{< pnote >}}
Gephi est un logiciel de visualisation et d'exploration de réseaux, très utilisé en histoire ou dans le domaine du journalisme.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
### 3.8. Gephi
{{< /pcache >}}

- fonction(s) : visualiser des graphes en appliquant un certain nombre de calculs
- usage(s) : donner une représentation graphique de réseaux
- création : 2008, notamment par Sébastien Heymann
- mise à jour : une ou plusieurs fois par an
- changement(s) induit(s) : visualiser des informations impossibles à 

{{< pnote >}}
Gephi permet d'appréhender la théorie des graphes et de comprendre pourquoi la visualisation a une place prépondérante dans certaines disciplines.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
{{< /pcache >}}
### 3.9. eScriptorium
> A project providing digital recognition of handwritten documents using machine learning techniques.


{{< image src="escriptorium.png" >}}

{{< pnote >}}
eScriptorium est un logiciel de reconnaissance de documents manuscrits utilisant les techniques du _machine learning_.
En d'autres termes, eScriptorium permet de transcrire une image d'un document manuscrit en un texte structuré et balisé en appliquant un modèle spécifique.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
### 3.9. eScriptorium
{{< /pcache >}}

- fonction(s) : 
- usage(s) : numérisation de documents anciens ou non nativement numériques pour une exploitation analytique
- création : 2019, Inria
- mise à jour : développements soutenus
- changement(s) induit(s) : prise en relativement rapide d'un outil puissant de conversion et de structuration de documents manuscrits

{{< pnote >}}
eScriptorium est un logiciel ou programme emblématique des humanités numériques : il s'agit de la mise en place d'un outil permettant la numérisation de documents manuscrits pour faciliter leur étude, par et pour la communauté scientifique.
Apprendre à utiliser eScriptorium permet de comprendre les enjeux de la numérisation assistée.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
{{< /pcache >}}
### 3.10. Ed (et GitHub Pages)
> Ed is a Jekyll theme designed for textual editors based on minimal computing principles, and focused on legibility, durability, ease and flexibility.

{{< image src="ed.png" >}}

{{< pnote >}}
Ed est un outil d'édition basé sur le générateur de site statique, il permet de créer _facilement_ un site web présentant des textes littéraires (quelque soit leur nature).
GitHub Pages permet de mettre en ligne des sites web dits statiques avec un coût très limité.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. 10 outils
### 3.10. Ed (et GitHub Pages)
{{< /pcache >}}

- fonction(s) : conversion de fichiers balisés, organisation de plusieurs pages de contenu, etc.
- usage(s) : mettre en ligne une édition numérique
- création : 2016, Alex Gil
- mise à jour : peu de mises à jour depuis 2021
- changement(s) induit(s) : structuration de donnée simplifiée pour une mise en ligne facile

{{< pnote >}}
Ed est basé sur une certaine approche du numérique et de l'informatique, et notamment le mouvement _low-tech_ ou le concept de _minimal computing_.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## Exercice
**Choisissez deux outils que vous aimeriez étudier et présenter.**

(En répondant à deux questions : pourquoi ce choix ? quel(s) usage(s) en avez/aurez-vous ?)

{{< psectiono >}}


{{< psectioni >}}
## 4. Qu'est-ce qu'une étude de cas ?

- analyse d'un objet/projet
- comprendre les implications scientifiques, techniques et fonctionnelles
- description et regard critique
- expérimenter

{{< pnote >}}
L'étude de cas est une analyse d'un objet ou d'un projet permettant d'en comprendre les implications scientifiques, techniques et fonctionnelles.
À la fois description référencée et sourcée, et regard critique, une étude de cas est un travail synthétique qui répond à des questions comme "qui ?", "comment ?", "pourquoi ?" ou "quand ?".
En décrivant les origines et le fonctionnement d'un objet, l'analyse de cas vise à expliquer l'intention initiale ainsi que de mesurer la pertinence de la démarche dans un contexte.

C'est un exercice qui requiert de la méthode, ainsi que des recherches et des manipulations.
Dans le cadre de ce cours en partie _pratique_, il est important de tester soi-même l'objet étudié afin d'en tirer une expérience.
{{< psectiono >}}
