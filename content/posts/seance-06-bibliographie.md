---
title: "Séance 06 - Bibliographer"
numero: "06"
date: 2022-08-29
date_p: 2022-10-13
lecture: "Collectif. (2018, 26 septembre). _Pourquoi Zotero?_ [Billet]. Le blog Zotero francophone. https://zotero.hypotheses.org/1998"
layout: diapositive
visible: true
---
{{< psectioni >}}
{{< pcache >}}

## Plan

1. Qu'est-ce qu'une bibliographie ?
2. Les contraintes de l'édition scientifique
3. Structurer une bibliographie : une nécessité
4. Zotero : présentation et exercices

{{< /pcache >}}
{{< psectiono >}}


{{< psectioni >}}
## 1. Qu'est-ce qu'une bibliographie ?

{{< figure src="bibliographie.png" legende="Capture d'écran d'un fichier CSL JSON" >}}

{{< pnote >}}
Une bibliographie est une liste structurée de références de documents, une bibliographie est utilisée pour référencer un certain nombre d'informations sur un sujet.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Qu'est-ce qu'une bibliographie ? 
{{< /pcache >}}
### 1.1. Qu'est-ce qu'un document ?

- un support + un contenu
- trois perspectives : forme, signe et médium

{{< pnote >}}
Un document est d'abord un support, un support qui _porte_ un contenu, et plus précisément tout type de savoir ou plus globalement d'information.
Il est difficile de définir très rapidement ce qu'est un document, toutefois nous pouvons retenir qu'il s'agit à la fois d'un support et d'un contenu (la bibliographie permet de décrire les deux), et que le document peut être considéré sous trois perspectives :

1. forme : le support ou la matérialité ;
2. signe : ce que signifie à la fois le contenu, le support et le type de média/médium ;
3. médium : un document n'est pas _evanescent_, il dépend du choix de média.

Le collectif Roger T. Pédauque donne des informations précises et critiques sur ce qu'est un document aujourd'hui, _Le document à la lumière du numérique_.
Un peu de Pédauque.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Qu'est-ce qu'une bibliographie ?
{{< /pcache >}}
### 1.2. Référencer pour trouver

- dans un monde d'information l'enjeu est de la trouver
- pour trouver : référencer
- plusieurs niveaux : individuel, collectif, global

{{< pnote >}}
C'est en partie la tâche des bibliothécaires : être capable de référencer des documents pour ensuite mettre à disposition ces références (mais pas directement les documents).
À un niveau plus individuel, et dans le domaine académique, il s'agit d'enregistrer des références pour ensuite pouvoir les partager, de plusieurs manières.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Qu'est-ce qu'une bibliographie ? 
{{< /pcache >}}
### 1.3. La difficulté d'un système commun

- facile : décrire un document
- moyen : structurer les métadonnées
- difficile : partager une description

{{< pnote >}}
Si décrire un document est relativement simple, notamment avec des données comme son titre, ses auteurs et autrices, son année de publication, etc., il est plus difficile de _structurer_ ces données pour pouvoir les exprimer clairement.
Le partage d'une description est donc la tâche la plus difficile.

Cette difficulté se réduit quand on sait quels types de document on doit décrire : livre, article de revue, chapitre, billet de blog, page web, etc.
Dans le champ universitaire les types de documents qui doivent être référencés sont relativement limités.

Il y a mille façons de décrire un document, c'est d'ailleurs une des premières choses que l'on apprend en bibliothéconomie : donner tous les éléments qui permettent d'identifier et de décrire un document quel qu'il soit.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Les contraintes de l'édition scientifique

{{< figure src="llm1300-schema-chaine-academique-03.png" legende="Schéma du circuit d'un document dans le processus d'édition d'un article scientifique" >}}

{{< pnote >}}
L'édition scientifique est un domaine très spécifique avec de nombreuses contraintes, entre les besoins de diffusion très divers, les chaînes de publication qui doivent prendre en considération de multiples paramètres ou encore le fait que les outils d'écriture ne prennent pas toujours en compte la richesse des contenus.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Les contraintes de l'édition scientifique 
{{< /pcache >}}
### 2.1. La structuration des contenus

- un document : des types de contenu
- structurer : donner du sens
- l'implicite des outils d'écriture (voir d'édition)

{{< pnote >}}
Dans _un_ document il y a _des_ types de contenu : un texte n'est pas qu'une suite de lettres et de mots, l'information est qualifiée.
Il s'agit par exemple d'identifier un paragraphe, une liste, une note de bas de page, une image, une légende ou encore une citation.
Structurer un document c'est lui donner du sens, c'est qualifier l'information qu'il contient.
Cette pratique de structurer ne doit pas intervenir _après_ l'écriture, c'est une partie de l'écriture elle-même (comme nous l'avons vu dans la séance précédente).

Les outils d'écriture qualifient l'information souvent implicitement : la sémantique est signifiée graphiquement à défaut de pouvoir être réutilisée.
Même certains outils d'édition ne prennent pas en compte cette question de la structuration.
La bibliographie fait aussi partie des oubliés.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Les contraintes de l'édition scientifique
{{< /pcache >}}
### 2.2. La circulation des textes

- des étapes nombreuses : soumission, révision, édition, fabrication, diffusion
- des réécriture permanentes : annotations, suggestions, modifications
- la nécessité de ne pas perdre la richesse sémantique

{{< pnote >}}
Les textes parcourent des circuits complexes, l'enjeu est de parvenir à conserver toutes les informations, et ce n'est pas simple.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Les contraintes de l'édition scientifique
{{< /pcache >}}
### 2.3. La publication multimodale

- au moins deux formats : PDF (imprimable) et XML
- une source unique
- une modélisation des formes


{{< pnote >}}
Les formats les plus utilisés dans la publication scientifique sont le PDF (format pensé pour l'impression alors que la plupart du temps il n'y a pas d'impression) et le format XML.
L'un est un format de consultation, l'autre est un format pivot qui permet aux plateformes de générer d'autres formats balisés comme le HTML pour les plateformes de diffusion numérique.
Le format HTML est ainsi rarement produit depuis une source, il est le résultat d'un long traitement documentaire, et il est généré automatiquement depuis le format XML grâce à des feuilles de transformation.

Certaines chaînes de publication permettent par ailleurs de produire plusieurs formats de sortie à partir d'une source unique : l'enjeu ici est de réduire les interventions et la circulation du texte.
Travailler à partir d'une source unique c'est ainsi privilégier le travail sur le texte et sa structuration, pour concentrer l'énergie nécessaire à la publication sur les règles de transformation ou les modèles de données en sortie.

La modélisation des formes consiste à définir la façon dont les données vont être transformées.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. Structurer une bibliographie : une nécessité

{{< figure src="bibliographie-manuelle.png" legende="Exemple d'une bibliographie manuelle dans LibreOffice" >}}

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Structurer une bibliographie : une nécessité 
{{< /pcache >}}
### 3.1. 10 352 styles bibliographiques

- en réalité plusieurs centaines réellement différents
- impossible de passer de Chicago à APA sans perte d'information
- impossible aussi de savoir quel style vous sera demandé demain

{{< pnote >}}
Il y a définitivement trop de styles existants, et donc il est trop risqué d'en choisir un et de devoir tout refaire à chaque changement de style bibliographique.
Par ailleurs certains styles intègrent plus d'informations que d'autres, il devient donc très délicat de passer par exemple de APA à Chicago (les prénoms des auteurs et des autrices seraient en effet perdus).
Enfin, dans le cas où l'on commence à rédiger une bibliographie pour une soumission, le risque est que le style soit modifié lors du dépôt final, il faudrait alors potentiellement tout reprendre à la main… (Sans parler du fait que certains styles sont tout simplement compliqués à écrire à la main.)
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Structurer une bibliographie : une nécessité 
{{< /pcache >}}
### 3.2. Naviguer dans les données

- classer, organiser, filtrer
- chercher
- tagger

{{< pnote >}}
Pouvoir explorer les données facilement.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Structurer une bibliographie : une nécessité
{{< /pcache >}}
### 3.3. Écrire et mettre à jour

- modifier facilement une bibliographie
- répercuter les modifications
- etc.


{{< pnote >}}
Avec l'enjeu de l'interopérabilité.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 4. Zotero : présentation et exercices 

{{< figure src="zotero-01.png" legende="Capture d'écran du logiciel Zotero" >}}

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Zotero : présentation et exercices
{{< /pcache >}}
### 4.1. Présentation de Zotero : description et objectifs

- un outil de gestion de références bibliographiques
- pour les références : importer, enregistrer, créer, organiser, partager
- pour les bibliographies : créer, modifier, partager

{{< pnote >}}
Zotero est un outil de gestion bibliographique, c'est un logiciel open source/libre, composé de deux 
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Zotero : présentation et exercices
{{< /pcache >}}
### 4.2. Fonctionnement de Zotero

- un logiciel de gestion : créer et organiser les références dans des collections et des groupes
- une extension pour navigateurs : pour importer des références depuis le Web
- les données sont conservées dans une base de données
- un service en ligne

{{< pnote >}}
En plus de proposer un logiciel et une extension, Zotero est aussi un service en ligne.
Un version plus succincte existe même pour une utilisation rapide sans compte : ZoteroBib ([https://zbib.org/](https://zbib.org/)).
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Zotero : présentation et exercices
{{< /pcache >}}
### 4.3. Et les autres logiciels ?

- _à la main_ : beaucoup de personnes préfèrent gérer des références manuellement
- BibTeX : une façon de structurée une bibliographie
- logiciels propriétaires : Mendeley, EndNote
- quelques alternatives libres : BibDesk, Wikindx ou JabRef

{{< pnote >}}
Il est difficile de trouver des concurrents sérieux à Zotero dans le domaine des sciences humaines.
Si Mendeley ou EndNote sont très utilisés dans d'autres domaines scientifiques, ils sont des outils qui coûtent chers et qui ne sont pas toujours 

Du côté des logiciels libres, les alternatives proposent souvent moins de fonctionnalités et surtout pas de service en ligne associé (pour sauvegarder ou partager des données).
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Zotero : présentation et exercices
{{< /pcache >}}
### 4.4. Quel futur pour Zotero ?

- une API puissante
- une alternative minimale avec ZoteroBib
- comment maintenir Zotero ?

{{< pnote >}}
Depuis les débuts Zotero est orienté vers le Web, mais cela se confirme avec l'amélioration de l'interface ainsi que la création d'outils comme ZoteroBib.
Malgré une ancienneté déjà conséquente (16 ans c'est beaucoup pour un logiciel), Zotero reste un gestionnaire de qualité.

La question qui pourrait être posée est celle du futur de Zotero à l'horizon 2030.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Zotero : présentation et exercices
{{< /pcache >}}
### 4.5. Exercice 1 : enregistrer et classer

- prérequis : créer un compte Zotero
- installation du logiciel Zotero + extension (selon votre navigateur)
- chercher et enregistrer 10 références sur les logiciels libres

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Zotero : présentation et exercices
{{< /pcache >}}
### 4.6. Exercice 2 : citer et bibliographer

- créer une collection et classer vos références dans ce dossier
- sélectionner une référence et faites CTRL+MAJ+A
- coller la citation dans un document Word/LibreOffice
- modifier le style bibliographique par défaut dans Édition > Préférences > Exportation
- essayer à nouveau de copier-coller une référence
- créer une bibliographie en faisant un clic droit sur la collection puis Créer une bibliographie…
- coller le résultat dans un document

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Zotero : présentation et exercices
{{< /pcache >}}
### 4.7. Exercice 3 : collaborer ou partager

- indiquer moi votre _username_ pour que je vous ajoute au [groupe HNU2000](https://www.zotero.org/groups/4751243)
- ajouter une référence sur le logiciel libre dans ce groupe
- créer un nouveau groupe et inviter moi (mon username : antoinentl)


{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Zotero : présentation et exercices
{{< /pcache >}}
### 4.8. Exercice 4 : exporter et sauvegarder

- installer l'extension BetterBibTeX : https://retorque.re/zotero-better-bibtex/installation/
- modifier le style des clés dans Édition > Préférences > BetterBibTeX > Citations Keys avec `zotero.clean`
- exporter une collection au format BetterBibTeX et cocher "garder à jour"

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}

