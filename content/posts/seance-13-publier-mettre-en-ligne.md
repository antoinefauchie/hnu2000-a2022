---
title: "Séance 13 - Publier et mettre en ligne"
numero: "13"
date: 2022-08-29
date_p: 2022-12-08
lecture: "Gil, A. (2019). Design for Diversity: The Case of Ed. Dans The Design for Diversity Learning Toolkit.. Northeastern University Library. Consulté à l’adresse https://des4div.library.northeastern.edu/design-for-diversity-the-case-of-ed-alex-gil/"
layout: diapositive
visible: true
---
{{< psectioni >}}
{{< pcache >}}

## Plan

1. L'édition numérique
2. Le principe du Single source publishing
3. Les apports du minimal computing
4. Jouons avec Ed.

{{< /pcache >}}
{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 1. L'édition numérique

{{< figure src="bookofthefuture.jpg" legende="Une illustration de Grant Snider" >}}

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. L'édition numérique
{{< /pcache >}}
### 1.1. Qu'est-ce que l'édition ?

- l'édition est un "processus" qui opère sur des "contenus" ;
- ce processus se divise en 3 "étapes" (3 fonctions) :
  1. fonction de choix et de production ;
  2. fonction de légitimation ;
  3. fonction de diffusion ;
- la maison d'édition : une instance éditoriale parmi d'autres.

{{< pnote >}}
Les étapes (ou fonctions) sont liées entre elles, sans l'une les autres ne peuvent pas seules porter l'édition.

La notion de processus implique une double dimension logique et temporelle : les étapes se suivent dans le temps et sont interdépendantes (si une étape n'a pas été réalisée, la suivante ne peut pas débuter).
Par la suite nous interrogerons à nouveau cette double dimension.

> Éditer signifie d’abord choisir et produire.

> La fonction de légitimation établit donc une différence entre les contenus et donne des indices sur leur valeur et finalement sur leur sens.

La fonction de diffusion est essentielle, car pour qu'un livre existe encore faut-il qu'il soit accessible ou disponible, et ce n'est pas une mince affaire.
Dans le cas d'un livre imprimé il s'agit de la chaîne du livre, et dans le cas d'un contenu numérique d'autres éléments sont à prendre en compte (comme le référencement web : la mise en ligne d'une page web ne suffit pas totalement à la faire exister).
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. L'édition numérique
{{< /pcache >}}
### 1.2. Les apports du numérique

- le numérique pour reproduire un fonctionnement analogique
- le numérique pour accélérer et faciliter
- le numérique pour repenser notre rapport au texte

{{< pnote >}}
La différence avec le numérique ?
Le numérique est encore très majoritairement utilisé pour reproduire un fonctionnement analogique, les capacités et les potentialités du numérique sont laissées de côté à cause d'une mauvaise maîtrise ou par crainte de remettre en cause un fonctionnement en place.

C'est en partie ce que décrivent Benoît Epron et Marcello Vitali-Rosati dans la partie "Enjeux stratégiques de la structuration des documents" de _L'édition à l'ère numérique_ :

- l'édition numérique fait appel à des technologies et des processus qui dépassent ceux utilisés de façon classique dans l'édition : le domaine de l'informatique se croise avec celui de l'édition par exemple ;
- d'autres types d'acteur interviennent dans le domaine du livre, alors que jusqu'ici ce n'était pas le cas ;
- les structures d'édition sont obligées d'adapter, côté production au moins, certaines pratiques issues de l'informatique.{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. L'édition numérique
{{< /pcache >}}
### 1.3. Formes et formats

- il n'y a pas _un_ livre numérique
- qu'est-ce qu'un format ?
- produire plusieurs formes/formats : une nécessité ?

{{< pnote >}}
Le terme _format_ est un terme technique qu'il est nécessaire de définir précisément : le format délimite les caractéristiques d'un objet.
Cette délimitation se traduit par un certain nombres de données, d'instructions, ou de règles.
Le but est de constituer une série d'informations compréhensible, utilisable et communicable.
Pour prendre un exemple concret, l'impression d'un document nécessite de s'accorder sur un format de papier.
Les largeurs, longueurs et orientations sont normalisées, des standards sont établis, ils permettent alors de concevoir des imprimantes qui peuvent gérer des types définis de papier.
Sans des formats de papier il est difficile de créer des machines adéquates, nous pouvons imaginer la complexité d'un appareil qui serait capable de s'adapter à n'importe quelle dimension de papier.
L'usage du format dans l'imprimerie est sans doute la première apparition de ce terme technique, le _format_ est ainsi d'abord attaché au livre et à sa fabrication.
Nous pouvons noter que des outils ou des processus sont associés au format : les instructions sont définies pour qu'une action soit réalisée par un agent — humain, analogique, mécanique, numérique.

Un _format informatique_ est une spécification technique, il structure des informations, il est le pivot entre une organisation logique et son implémentation dans un système informatique.
Un fichier doit avoir un format, sans quoi il ne pourra être produit, transmis ou lu.
Un format informatique est le lien entre l'infrastructure et la personne qui utilise cette infrastructure.
Le choix des formats informatiques détermine la manière dont les informations sont créés, stockées, envoyées, reçues, interprétées, affichées.
Autant dire qu'aujourd'hui ils occupent une place importante dans notre environnement, et leur incidence dépasse le domaine de l'informatique, leur étude a pourtant été longtemps délaissée dans le champ des médias.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Le principe du Single source publishing

{{< figure src="ssp.png" legende="Le Single source publishing en un schéma" >}}

{{< pnote >}}
Ce schéma complet permet de comprendre toutes les dimensions du _single source publishing_.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Le principe du Single source publishing
{{< /pcache >}}
### 2.1. Une seule source

- **une source unique**
- pour éviter les confusions
- pour gagner en énergie
- mais différents types d'intervention

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Le principe du Single source publishing
{{< /pcache >}}
### 2.2. Modéliser les sorties

- un format de sortie = un modèle/_template_ spécifique
- répartir chaque information
- prendre en compte un certain nombre de contraintes

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Le principe du Single source publishing
{{< /pcache >}}
### 2.3. Une approche modulaire

- penser une architecture technique qui s'adapte
- ne pas s'enfermer dans des modèles contraignants
- exemple de Quire

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. Les apports du minimal computing

{{< figure src="minimal-computing.jpg" legende="Illustration d'un boulier, British Library" >}}

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Les apports du minimal computing
{{< /pcache >}}
### 3.1. Définition du concept

> Minimal computing is an approach that, first and foremost, advocates for using only the technologies that are necessary and sufficient for developing digital humanities scholarship in such constrained environments. This does not mean that the “minimal” of “minimal computing” implies ease for all users or prescribes acceptable types of hardware, software, and platforms (e.g., Jekyll, Arduino, and Raspberry PI).[3] Rather, it gestures towards a decision-making process driven by the local contexts in which scholarship is being created. In this way, minimal computing is platform- and software-agnostic, emphasizing instead the importance of making these choices, based on the constraints with which we are working, to facilitate the development of digital humanities scholarship in environments where resources (e.g., financial, infrastructural, and labor) or freedoms (e.g., movement and speech) are scarce.  
> Roopika Risam et Alex Gil, [The Questions of Minimal Computing](http://www.digitalhumanities.org//dhq/vol/16/2/000646/000646.html)

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Les apports du minimal computing
{{< /pcache >}}
### 3.2. Retour sur l'article d'Alex Gil
Gil, A. (2019). Design for Diversity: The Case of Ed. Dans The Design for Diversity Learning Toolkit.. Northeastern University Library. Consulté à l’adresse https://des4div.library.northeastern.edu/design-for-diversity-the-case-of-ed-alex-gil/


{{< pnotec >}}
La première partie est un préambule important : la question de la diversité est complexe, et elle n'est pas négociable.
Le manque de ressources, les courbes d'apprentissage et le manque de temps sont également des freins à une plus grande diversité.
Pour remédier à cela, l'approche du _minimal computing_ est censée faciliter la mise en place de certains projets et leur prise en main par tous et toutes.

Alex Gil présente ensuite rapidement _Ed_ et les objectifs qu'il est censé remplir.

Enfin Alex Gil détaille les différents points essentiels :

1. contrôle : la maîtrise de l'outil
2. énergie : l'empreinte écologique de l'outil et de ce qu'il produit
3. bande-passante et données : un simple site statique
4. l'Internet : fonctionnement possible sans accès à Internet
5. les mainteneurs : facile à mettre à jour, notamment en l'absence de base de données
6. les lecteurs : la lisibilité comme premier argument
7. les apprenants : apprendre à utiliser Ed demande un peu de temps, mais ce n'est pas du temps perdu !
8. les éditeurs : peu d'apprentissage nécessaire en dehors de Markdown
9. point financier : très peu de coûts financiers en entrée
10. conservation : les sources autant que les fichiers produits se conservent facilement et pour longtemps

{{< /pnotec >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Les apports du minimal computing
{{< /pcache >}}
### 3.3. Quelques éléments à garder en tête

- un accès/une maîtrise facile
- des technologies avec une empreinte faible
- se concentrer sur les objectifs du projet (et non un idéal technologique)
- aspect financier important
- peu de compromis sur le résultat final

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 4. Démonstration de Ed.


{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}

