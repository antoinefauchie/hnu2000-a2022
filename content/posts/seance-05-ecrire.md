---
title: "Séance 05 - Écrire"
numero: "05"
date: 2022-08-29
date_p: 2022-10-06
lecture: "Vitali-Rosati, M., Sauret, N., Fauchié, A. et Mellet, M. (2020). _Écrire les SHS en environnement numérique. L’éditeur de texte Stylo_. Revue Intelligibilité du Numérique. http://intelligibilite-numerique.numerev.com/numeros/n-1-2020/18-ecrire-les-shs-en-environnement-numerique-l-editeur-de-texte-stylo"
layout: diapositive
visible: true
---
{{< psectioni >}}
{{< pcache >}}

## Plan

1. Qu'est-ce que l'écriture (numérique) ?
2. Uniformisation des outils d'écriture numérique
3. Une courte histoire du balisage
4. Stylo en pratique

{{< /pcache >}}
{{< psectiono >}}


{{< psectioni >}}
## 1. Qu'est-ce que l'écriture (numérique) ?

{{< figure src="sapho.jpg" legende="Femme avec une tablette de cire et un stylet (dite « Sappho »), Herkulaneischer Meister" >}}

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Qu'est-ce que l'écriture (numérique) ?
{{< /pcache >}}
### 1.1. Définition de l'écriture

> L’écriture est un moyen de communication qui représente le langage à travers l'inscription de signes sur des supports variés.  
> Source : [Wikipédia](https://fr.wikipedia.org/wiki/%C3%89criture)

{{< pnote >}}
La suite de la citation est aussi très intéressante : "C'est une forme de technologie qui s'appuie sur les mêmes structures que la parole, comme le vocabulaire, la grammaire et la sémantique, mais avec des contraintes additionnelles liées au système de graphies propres à chaque culture."

Entre _moyen_ et _technologie_, l'écriture est donc un consensus reposant sur des techniques permettant d'échanger des informations.
L'écriture a ceci de particulier qu'elle est un moyen de rendre visible "la langue des hommes" (pour reprendre les propos et les mots d'Anne-Marie Christin), mais ce n'est pas une simple conversion de l'oral à l'inscription physique.
À ce sujet il y a deux approches opposées de l'écriture :

- celle d'André Leroi-Gourhan qui considère que l'écriture est un moyen de traduire la voix et donc la pensée. Le geste et la parole se rencontre : l'écriture est une trace ;
- celle d'Anne-Marie Christin qui défend une autre approche : l'écriture est un signe et non une trace, avec deux aspects qui la caractérisent : l'aspect matériel (une inscription) et l'aspect relationnel (inscription en lien avec autre chose).

Trace ou signe, il convient de prendre en considération les conditions d'émergence de l'écriture comme un tissu complexe de relations, de techniques et de pratiques.

Par ailleurs lorsqu'on utilise le terme ou concept d'écriture dans un contexte occidental nous faisons le plus souvent référence au système d'écriture alphabétique même s'il en existe bien d'autres.


{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Qu'est-ce que l'écriture (numérique) ?
{{< /pcache >}}
### 1.2. Écriture numérique ?
> le sens n’est pas le produit d’un être humain qui pense – l’idée de cognition internaliste –, mais d’une dynamique faite d’interactions sociales, culturelles, techniques et technologiques.  
> Vitali-Rosati, M. (2020). Qu’est-ce que l’écriture numérique ? _Corela. Cognition, représentation, langage_, (HS-33). https://doi.org/10.4000/corela.11759

{{< pnote >}}
L'écriture (numérique) est donc finalement l'écriture, mais dans un espace modifié.
La constitution de cet espace (qui n'est pas _séparé_ de l'espace physique) dépend de nombreux paramètres, il s'agit d'un espace matériel (serveurs, câbles, écrans, dispositifs de lecture-écriture), mais sur lequel nous n'avons pas la même prise.

Pour Marcello Vitali-Rosati, l'écriture est inhumaine, qu'elle soit _numérique_ ou non.
Dans ce long article érudit et passionnant, Marcello donne quelques caractéristiques de l'écriture numérique :

- écriture numérique = écriture technique
- "l'écriture numérique prend donc une valeur architecturale"
- l'écriture numérique est une suite de protocoles
- "le texte numérique est un espace de convergence"

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Qu'est-ce que l'écriture (numérique) ?
{{< /pcache >}}
### 1.3. Et les outils ?
Des outils qui réalisent des fonctions :

- inscrire
- enregistrer
- structurer
- mettre en forme
- partager
- publier
- diffuser
- etc.

{{< pnote >}}
Rares sont les outils qui parviennent à satisfaire toutes ces fonctions (ce qui n'aurait de toute façon pas forcément de sens).
Si nous devions retenir trois fonctions minimale ce serait probablement celles-ci :

- inscrire : autrement dit utiliser un système d'écriture, une convention pour pouvoir décoder des signes (ce qui demande déjà une certaine littératie) ;
- enregistrer : disposer d'un support qui permet de garder la trace, l'inscription. Ce support est forcément physique, mais il peut être plus ou moins accessible par l'humain : du papier lisible sans intermédiaire, ou un disque dur qui nécessite un ordinateur, un logiciel, un écran, etc.
- mettre en forme : c'est la condition pour une lisibilité du texte. Il est nécessaire de distinguer _visuellement_ ou _graphiquement_ les niveaux d'information, l'organisation du texte, sa structure.

Les autres fonctions découlent de ces trois-là : inscrire n'est possible que s'il y a structuration ; enregistrer implique implicitement une forme de partage, au moins avec soi-même (retrouver le fichier notamment) ; mettre en forme facilite la lecture d'un texte pour soi-même, mais pour peu que les _codes_ utilisés soient les mêmes qu'un groupe de personnes, alors la publication et la diffusion sont très proches.

À partir de cela difficile d'établir un panorama de tous les outils d'écriture (numérique) : de l'éditeur de texte comme le bloc note de Windows au CMS comme WordPress, en passant par le traitement de texte, les outils d'écriture sont nombreux.

Par la suite nous nous intéressons plus spécifiquement à l'écriture académique : les éléments présentés jusqu'ici sont valides pour ce domaine plus spécifique de l'écriture, mais les suivants sont bien moins généralistes.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Uniformisation des outils d'écriture numérique

{{< figure src="word.png" legende="Le mot Bonjour dans les méandres d'un fichier docx" >}}

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Uniformisation des outils d'écriture numérique
{{< /pcache >}}
### 2.1. Écrire sur : les premiers traitements de texte

{{< imageg src="electric-pencil.jpg" legende="Michael Shrayer et son outil Electric Pencil." >}}

{{< pnote >}}
L'origine du traitement de texte (entendu comme un logiciel bureautique permettant d'écrire et de mettre en forme un texte) est intéressante, en effet WordText naît dans les mains de Michael Shraver (avec Electric Pencil) après le constat suivant : pour les développeurs qui écrivent du code dans les années 1970, le seul moyen de documenter ce code est de taper à la machine à écrire _à côté_ de l'ordinateur.
Electric Pencil (1976) n'est pas à proprement parlé le premier traitement de texte, dans le sens où ce logiciel était d'abord pensé pour écrire de la documentation.
C'est à partir du milieu des années 1970 que les premiers traitements de texte font donc leur apparition, WordStar étant l'un de ceux qui dominera le marché pendant plusieurs années (il est encore utilisé aujourd'hui par quelques écrivains).
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}

{{< figure src="wordstar.png" legende="Capture d'écran du logiciel WordStar." >}}

{{< pnote >}}
Ce qui frappe en regardant à quoi ressemble WordStar, c'est le fait qu'un balisage est nécessaire pour _écrire_.
On peut noter que pour faire du gras, de l'italique ou pour changer la mise en forme du texte, des signes comme `^B` sont nécessaires.

S'en suit une guerre des traitements de texte, que Matthew Kirschenbaum raconte avec beaucoup de détails dans _Track Changes_, et dont le (presque) seul vainqueur est Microsoft Word.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Uniformisation des outils d'écriture numérique
{{< /pcache >}}
### 2.2. Word : un outil de souffrance

- confusion fond/forme
- logiciel propriétaire
- des outils absents

{{< pnote >}}
Word, et tout autre traitement de texte, pose plusieurs problèmes dont nous pouvons retenir les points suivants :

- le fait de disposer d'une interface graphique et d'un mode WYSIWYG (What You See Is What You Get, ou ce que vous voyez est ce que vous obtenez) entraîne une confusion entre le fond (le texte et sa structure logique ou sémantique) et la forme (le rendu graphique, la transcription visuelle de la structure). Plutôt que de qualifier le texte, on lui attribue des caractéristiques graphiques ;
- un logiciel propriétaire est un logiciel dont le code source n'est ni visible, modifiable ou partageable. Dans le cas d'un outil d'écriture utilisé par une communauté comme les chercheur·e·s, cela pose un certain nombre de problèmes ;
- enfin Word ou d'autres traitements de texte n'intègrent pas des outils essentiels à la rédaction scientifique (ou à la littérature, mais nous n'en parlons pas ici) : 

Le problème de Word tient principalement dans le fait que c'est un outil qui se destine très rapidement à la bureautique — alors que c'était un outil d'écriture littéraire dans les premières versions.

Tous les efforts mis dans l'utilisation de Word (et il en faut, des efforts) sont un investissement cognitif inutile, puisque l'apprentissage ne sert qu'à utiliser Word.
D'autre approche permettent la constitution d'une littératie numérique, c'est-à-dire le double apprentissage de lecture et écriture ainsi que la maîtrise d'outils 

Je ne dis pas que Word (ou d'autres traitements de texte) ne sont pas des outils éviter absolument, ils sont puissants et comportent nombre de fonctionnalités intéressantes.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Uniformisation des outils d'écriture numérique
{{< /pcache >}}
### 2.3. La réplication de Word

- LibreOffice Writer : l'argument du libre
- Google Docs : l'édition collaborative simultanée
- des alternatives avec de nouveaux modèles : iA Writer, Zettlr, Scrivener, pads (Hedgedoc)

{{< pnote >}}
Aujourd'hui il est difficile de dire que Word est le traitement de texte le plus utilisé, tant les usages se sont déplacés sur un concurrent qui propose une fonctionnalité redoutable : l'édition en ligne, à plusieurs, en temps réel.
Plus besoin de se soucier de savoir où est le fichier, il suffit de se connecter à son compte Google.

Google Docs est une application, le terme application correspond ici à une forme d'asservissement des utilisateurs et des utilisatrices : le service proposé par Google va plus loin que l'édition d'un document, il enferme les pratiques d'écriture dans son espace (fermé).

Comme Microsoft, Google propose un outil de bureautique plutôt qu'un dispositif d'écriture.
Il faut tout de même noter qu'un effort important est apporté pour tenter de simplifier l'interface graphique pour la rendre bien plus légère que celle de Word.

Il existe toutefois des alternatives, Matthew Kirschenbaum en parle dans _Track Changes_ : alternatives _brutes_ avec un retour au format texte (exemple de iA Writer), un déplacement sur le web avec des services/applications en ligne comme Google Docs ou les pads, ou encore des logiciels spécialisés comme Zettlr (pour l'écriture scientifique) ou Scrivener (pour les scénarios notamment).
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. Une courte histoire du balisage
```
<chapter><title>Introduction to SGML</title>
<section><title>The SGML Declaration</title>
<subsection>
```

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Une courte histoire du balisage
{{< /pcache >}}
### 3.1. SGML, HTML et XML

- SGML : décrire avec des balises
- HTML : structurer pour _afficher_
- XML : l'arbre sémantique

{{< pnote >}}
L'histoire des langages de balisage est une histoire longue, passionnante et complexe.
Sans rentrer dans tous les détails, nous pouvons observer plusieurs évolutions et divergences intéressantes.

SGML est d'abord conçu pour résoudre un problème lié la représentation et la reproduction des documents.
Jusqu'ici basé sur le langage Script pensé pour l'impression, les formats de document ne sont pas structuré (à part quelques exceptions comme TeX).
SGML (précédé par GML), pour Standard Generalized Markup Language, introduit donc le balisage déclaratif permettant de générer des documents graphiques.

HTML est une application de SGML, plus simple et plus adapté à un affichage graphique qui ne dépend pas du système de la page imprimée.
Il y a plusieurs versions successives de HTML, il faut retenir qu'il s'agit d'un ensemble d'éléments qui prennent en compte l'hypertexte.
Le langage HTML est accompagné de HTTP et du système d'adresses web (les URLs).

Enfin XML est développé à partir des années 1990 dans un objectif de rendre plus générique un langage de balisage.
XML est une suite de spécifications qui peuvent ensuite être implémentées dans des _schémas_, comme la TEI qui a d'ailleurs précédé XML.
XML est puissant mais aussi complexe à utiliser, il est aujourd'hui très éloigné de la version actuelle de HTML.
Ou disons que les choix techniques de HTML fait que ce langage est très permissif, ne respectant pas certaines prérogatives de SGML ou de XML.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Une courte histoire du balisage
{{< /pcache >}}
### 3.2. Le format texte
> Un fichier au format texte est un fichier dont le code binaire (suite de 0 et de 1) se traduit par des caractères textuels uniquement (par opposition à des pixels ou des sons par exemple). Dit de manière encore plus courte : c’est un fichier qui ne contient des caractères.  
> Arthur Perret, Format texte, [https://www.arthurperret.fr/cours/format-texte.html](https://www.arthurperret.fr/cours/format-texte.html)

{{< pnote >}}
Arthur Perret propose une _fiche_ très synthétique et très complète qui définit le format texte.

Le format texte, ou aussi appelé texte brut (de l'anglais _plain text_), a beaucoup d'avantage, et notamment :

- la lisibilité : contrairement aux formats binaires qui ne sont lisibles que grâce à un logiciel spécifique (comme le format .doc, et non le format .docx) ;
- la légèreté : les fichiers au format texte sont souvent très légers, ne dépassant pas quelques kiloctets même avec des grandes quantités de texte, puisqu'il ne comporte que du texte. Cela induit une performance : rapide à lire par un programme, peu de place sur un support de stockage, utilisable par n'importe quel système d'exploitation ;
- la pérennité : un fichier informatique au format texte sera toujours lisible dans 10 ans, quand bien même les applications dédiées à son interprétation ne seront plus disponibles ;
- et dans une certaine mesure l'interopérabilité : un format texte est dépendant d'une spécification qu'il est possible d'implémenter 

Enfin, le format texte est libre : pour l'ouvrir, le lire, le modifier ou le transmettre il n'est pas nécessaire de payer une licence.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Une courte histoire du balisage
{{< /pcache >}}
### 3.3. Les langages de balisage léger

```
# Un titre de niveau un
Du texte, ici **du gras**, là _de l'italique_.

> Une citation  
> [Un lien](https://example.com/lien)
```

{{< pnote >}}
Le qualificatif "léger" permet de comprendre de quoi il s'agit face aux autres langages précédemment cités qui peuvent être jugés _lourds_.
Grâce à une économie de caractères ou de signes pour baliser le texte, certains langages de balisage sont humainement lisibles (contrairement à HTML ou XML qui sont très verbeux, à juste titre).
Avec quelques signes typographiques, des langages comme Markdown ou AsciiDoc permettent de faire de la sémantique avec un apprentissage minime.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 4. Stylo en pratique

{{< figure src="stylo.png" legende="Capture d'écran de Stylo" >}}

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Stylo en pratique
{{< /pcache >}}
### 4.1. Préambule : qu'est-ce que Stylo ?
Discussion autour de l'article :

Vitali-Rosati, M., Sauret, N., Fauchié, A. et Mellet, M. (2020). _Écrire les SHS en environnement numérique. L’éditeur de texte Stylo_. Revue Intelligibilité du Numérique. http://intelligibilite-numerique.numerev.com/numeros/n-1-2020/18-ecrire-les-shs-en-environnement-numerique-l-editeur-de-texte-stylo

{{< pnotec >}}
Quelques notes sur cet article :

- il s'agit de s'interroger sur les outils d'écriture en tant que structurant la pensée : "Les logiciels – et plus généralement le code – ne sont pas neutres : ils portent et promeuvent des valeurs, et conditionnent ainsi le développement de nos sociétés." ;
- les logiciels d'écriture font encore trop peu l'objet de recherches, alors que ce sont précisément ces outils qui structurent la recherche en sciences humaines ;
- Stylo renverse le paradigme actuel : plutôt que de travailler avec des outils WYSIWYG qui donnent au texte un rendu graphique censé représenter une sémantique, il s'agit d'_écrire_ cette sémantique à l'aide d'un balisage dit léger, Markdown ;
- plutôt que d'apprendre à utiliser un outil qui ne nous apprend rien, autant apprendre à structurer un texte à l'aide balises ;
- les 3 principes de Stylo : maîtriser l'expression de la sémantique du texte ; produire des données structurées et riches ; "utiliser des standards et des technologies simples et pérennes" ;
- écrire dans l'environnement numérique c'est déjà faire de l'édition.

Enfin un point essentiel : démarche de recherche-action : Stylo n'est pas une _application_ proposée par une entreprise, il n'y a aucun profit recherché, voir aucun succès **du tout** (si n'est expliquer l'intérêt d'une telle démarche).
{{< /pnotec >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Stylo en pratique
{{< /pcache >}}
### 4.2. Découvrir Markdown
Tutoriel en ligne :

[https://www.arthurperret.fr/tutomd/](https://www.arthurperret.fr/tutomd/)

{{< pnote >}}
Tutoriel original en anglais : https://commonmark.org/help/

La traduction est de Arthur Perret (doctorant à l'Université Bordeaux Montaigne).

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Stylo en pratique
{{< /pcache >}}
### 4.3. Créer un document Stylo

- créez-vous un compte HumanID si ce n'est pas encore fait : https://humanid.huma-num.fr/
- accédez à Stylo : https://stylo.huma-num.fr/
- créez un nouvel article
- le texte doit comporter au moins : 2 niveaux de titre, des liens, une citation, plusieurs paragraphes et parties, une référence bibliographique
- envoyez-moi le lien de prévisualisation lorsque vous avez terminé : antoine.fauchie@umontreal.ca

{{< pnote >}}
Vous pouvez utiliser un texte déjà existant.

Pour gagner du temps et si vous n'avez pas de compte Zotero, vous pouvez utiliser la référence bibliographique au format BibTeX suivante :

```
@article{vitali-rosati_ecrire_2020,
	title = {Écrire les {SHS} en environnement numérique. {L}’éditeur de texte {Stylo}},
	url = {http://intelligibilite-numerique.numerev.com/numeros/n-1-2020/18-ecrire-les-shs-en-environnement-numerique-l-editeur-de-texte-stylo},
	abstract = {Résumé : L’écriture et l’édition scientifiques en contexte numérique sont relativement peu interrogées ou remises en cause. Quelques outils, qui...},
	language = {fr-fr},
	urldate = {2020-10-08},
	journal = {Revue Intelligibilité du Numérique},
	author = {Vitali-Rosati, Marcello and Sauret, Nicolas and Fauchié, Antoine and Mellet, Margot},
	year = {2020},
	file = {Snapshot:/home/antoine/Zotero/storage/XPFVIJQG/18-ecrire-les-shs-en-environnement-numerique-l-editeur-de-texte-stylo.html:text/html},
}
```
{{< /pnote >}}
{{< psectiono >}}

