---
title: "Séance 01 - Définir les humanités numériques"
numero: "00"
date: 2022-08-29
date_p: 2022-09-08
bibfile: data/bibliographie.json
visible: true
reference: true
layout: diapositive
lecture: "Sinatra, M. & Rosati, M. (2014). Histoire des humanités numériques. Dans Pratiques de l’édition numérique. Les Ateliers de [sens public]. Consulté à l’adresse https://www.parcoursnumeriques-pum.ca/1-pratiques/chapitre3.html"
---
{{< psectioni >}}
## Plan de la séance

{{< psectiono >}}


{{< psectioni >}}
## 1. Nos définitions de "humanités numériques"
Quelle est votre définition en une phrase ?

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Nos définitions de "humanités numériques"
{{< /pcache >}}
791 définitions !

{{< psectiono >}}


{{< psectioni >}}

> The ineffably sublime.  
> Stéfan Sinclair

{{< psectiono >}}


{{< psectioni >}}

> I like to define Digital Humanities as a set of new activities for humanists instead of a field of study, because Digital Humanities involve something more than a methodology or an interest on a topic. Implies a complete different attitude towards humanities, the understanding, use and creation of new tools, and a alternative way to communicate the findings. This is something that involves you completely.  
> Ernesto Priani

{{< psectiono >}}


{{< psectioni >}}

> The use of digital tools, media and corpora in the humanities, thus changing both the objects and practices in the Humanities.  
> Thomas Lebarbé

{{< pnote >}}
C'est la définition qui me semble le mieux convenir ici, elle est claire et concise.
Voici une proposition de traduction :

> L'utilisation d'outils, de médias et de corpus numériques dans les sciences humaines, modifiant ainsi à la fois les objets et les pratiques des sciences humaines.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}

> The digital humanities is the use of digital methods and technologies in humanities teaching and research. Humanities computing, by contrast, can be reserved for the study and development of those methods and technologies.  
> Geoffrey Rockwell

{{< psectiono >}}


{{< psectioni >}}

> It is both a methodology and a community.  
> Jason Farman

[https://whatisdigitalhumanities.com/](https://whatisdigitalhumanities.com/)
{{< pnote >}}

En cherchant parmi ces 791 définitions, il est très intéressant de noter que le mot "tool" ou "tools" revient 248 fois, ou le terme "technology" ou "technologies" 350 fois. 
Cela signifie que la question de la technique est particulièrement importante dans l'approche des humanités numériques.

Ce site web est lui-même une approche des humanités numériques :

- constituer un corpus de textes à partir de données structurées (ici un tableur au format CSV) ;
- proposer une lecture innovante de ces 791 textes par un dispositif d'interface homme-machine : en rafraichissant la page je peux accéder aux différentes définitions ;
- mettre en place un processus de publication permettant différents types de contributions ;
- repenser le concept de définition avec un accès aléatoire aux définitions.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Humanités ?

{{< psectiono >}}


{{< psectioni >}}

{{< imageg src="livre.jpg" >}}

{{< pnote >}}
- traditionnellement : lettres classiques
- aujourd'hui (et surtout en Amérique du nord) : les sciences humaines
- littérature, philosophie, histoire, arts vivants, linguistiques, etc.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. Numérique ?


{{< psectiono >}}


{{< psectioni >}}

{{< imageg src="ordinateur.jpg" >}}

{{< pnote >}}
- représentation par nombre
- discrétisation
- numérisation du monde
- culture numérique
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 4. Sciences humaines + Informatique ?

# DH = 📖 + 💻 ?

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Sciences humaines + Informatique ?
{{< /pcache >}}
Plutôt qu'une addition : une conjonction

{{< pnote >}}
Une addition : il s'agit d'une réunion d'éléments, deux choses se cotoient sans forcément être en relation.

Une conjonction : il s'agit d'un endroit où deux choses se joignent, il y a une rencontre, le partage d'un chemin commun, mais aussi et surtout une forme d'union qui produit un effet.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 5. Un peu d'histoire
Lecture pendant le cours :

Sinatra, M. & Rosati, M. (2014). Histoire des humanités numériques. Dans _Pratiques de l’édition numérique_. Les Ateliers de [sens public]. Consulté à l’adresse https://www.parcoursnumeriques-pum.ca/1-pratiques/chapitre3.html

{{< pnotec >}}
En lisant ce chapitre, plusieurs éléments peuvent être notés :

- il faut considérer deux "niveaux" dans l'approche des humanités numériques : l'utilisation des ressources informatiques (méthodes et puissance de calcul) pour les sciences humaines d'une part, et le fait d'analyser notre environnement fortement influencé par le numérique d'autre part ;
- l'avènement du numérique (informatique, ordinateurs personnels et Internet/Web) s'est fait depuis la recherche d'un dispositif permettant de lier des documents, d'organiser le savoir de façon plus dynamique (ce que ne permet pas totalement l'analogique) ;
- la période des index : les recherches scientifiques en linguistique commencent à utiliser la puissance de calcul des ordinateurs ;
- depuis la _literary and linguistic computing_ aux _digital humanities_ en passant par les _humanities computing_ : l'informatique n'est plus centrale ;
- avec l'arrivée d'Internet puis du Web il devient évident que notre rapport au savoir est complètement modifié, les sciences humaines doivent s'emparer de ce changement et l'analyser ;
- "Les humanités numériques doivent développer une réflexion sur la façon dont les outils numériques changent la recherche en sciences humaines" ;
- "Le numérique est-il le sujet ou l’objet des humanités numériques ?" Cette question est déterminante pour comprendre les humanités numériques ;
- d'une certaine façon les humanités numériques sont la rencontre, la _conjonction_, de la théorie (penser le numérique) et de la pratique (concevoir et utiliser des outils) ;
- il faut éviter l'écueil du déterminisme technologique ;
- le lien entre humanités numériques et _édition_ numérique est très fort.

{{< /pnotec >}}
{{< psectiono >}}


{{< psectioni >}}
## 6. Ce qu'il faut retenir

- une approche globale interdisciplinaire
- **des** définitions
- conjonction
- réflexion et pratique


{{< pnote >}}
- les humanités numériques peuvent être considérer comme une approche globale interdisciplinaire ;
- plusieurs définitions des humanités numériques peuvent cohabiter, et doivent cohabiter ;
- il faut considérer une conjonction entre humanités et numériques plutôt qu'une addition ;
- il y a une nécessité à nous interroger sur le numérique et à créer des outils pour faire des humanités.

{{< /pnote >}}
{{< psectiono >}}

