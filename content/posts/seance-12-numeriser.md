---
title: "Séance 12 - Numériser"
numero: "12"
date: 2022-08-29
date_p: 2022-12-01
lecture: "Chagué, A. (2022, mai). Intelligence Artificielle et intelligence collective : des nouveaux eldorados pour rendre les textes patrimoniaux plus accessibles ? _Museonum_. https://medium.com/museonum/intelligence-artificielle-et-intelligence-collective-des-nouveaux-eldorados-pour-rendre-les-c8c4e214d4e6<br>Stokes, P., Kiessling, B., Stökl Ben Ezra, D., Tissot, R. et Gargem, E. H. (2021). The eScriptorium VRE for Manuscript Cultures. Classics@ Journal, 18. https://classics-at.chs.harvard.edu/classics18-stokes-kiessling-stokl-ben-ezra-tissot-gargem/"
layout: diapositive
visible: true
---
{{< psectioni >}}
{{< pcache >}}

## Plan

1. La numérisation : enjeux et techniques
2. Apprendre aux machines à apprendre

{{< /pcache >}}
{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 1. La numérisation : enjeux et techniques

{{< figure src="a-la-recherche.png" legende="À la recherche du temps perdu de Marcel Proust" >}}

{{< pnote >}}
Les questions de numérisation sont complexes, comme nous l'avons vu dans des séances précédentes.
Ces questions sont autant théoriques, notamment sur le statut du texte, sa transmission et sa réception, mais aussi techniques.
Certaines technologies sont invoquées pour permettre une meilleure compréhension et une meilleure diffusion de corpus imprimés ou manuscrits, anciens ou contemporains.
Comme souvent dans le cas des humanités numériques, ce ne sont pas tant de nouveaux moyens qui sont à disposition des chercheur·e·s que de nouvelles façons de considérer le travail même de recherche voir le rapport aux textes analysés et étudiés.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. La numérisation : enjeux et techniques
{{< /pcache >}}
### 1.1. Pourquoi numériser ?

- pour découvrir
- pour lire
- pour chercher

{{< pnote >}}
La démarche de numérisation revêt plusieurs objectifs qui concernent plusieurs types de publics.
Que ce soit la découvrabilité (majoritairement pour le _grand public_), la lecture (pour tous les publics) ou la recherche (pour des publics érudits ou académiques), plusieurs niveaux d'accès aux documents numérisés sont possibles.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. La numérisation : enjeux et techniques
{{< /pcache >}}
### 1.2. Numérisation et OCR

- travail préparatoire : cataloguer, organiser, signaler, référencer, sélectionner
- prise de vue : les contraintes du papier et des livres
- reconnaissance optique de caractères : _ça dépend_
- bonus : structuration (zonage, etc.)

{{< pnote >}}
La numérisation est un processus complexe, qui comporte plusieurs étapes.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. La numérisation : enjeux et techniques
{{< /pcache >}}
### 1.3. De l'OCR au HTR
HTR = Handwritten Text Recognition

- OCR : caractères imprimés : ça marche !
- HTR : écritures manuscrites : c'est compliqué
- la nécessité d'entraîner des machines

{{< pnote >}}
L'OCR est une opération qui est effectuée sur des corpus imprimés, donc avec a priori une lisibilité relativement bonne (par rapport à des documents manuscrits par exemple), même si certains documents présentent des difficultés comme la presse.
Aujourd'hui les procédés d'OCR donnent de très bons résultats.

Pour le HTR, ou _Handwritten Text Recognition_, les résultats sont plus délicats, et ils ne peuvent pas reposer que sur des algorithmes.
Il faut entraîner des machines à reconnaître certains types de texte.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Apprendre aux machines à apprendre

{{< figure src="stendhal.png" legende="" >}}

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Apprendre aux machines à apprendre
{{< /pcache >}}
### 2.1. Pourquoi entraîner des machines ?
Focus sur l'article d'Alix Chagué :

Chagué, A. (2022, mai). Intelligence Artificielle et intelligence collective : des nouveaux eldorados pour rendre les textes patrimoniaux plus accessibles ? _Museonum_. https://medium.com/museonum/intelligence-artificielle-et-intelligence-collective-des-nouveaux-eldorados-pour-rendre-les-c8c4e214d4e6

{{< pnotec >}}

Dans cet article Alix Chagué explique la complémentarité nécessaire entre plusieurs méthodes.

> L’un des domaines d’application de l’IA les plus pertinents pour la documentation des collections de musées est sans nul doute celui de la vision par ordinateur (computer vision). Il s’agit de permettre aux machines de recevoir un input visuel fixe ou animé (image ou vidéo) et de l’interpréter de manière à en extraire des informations telle que le thème représenté ou encore la présence d’objets ou de motifs et leur position dans l’espace. Il s’agit, en fait, tout simplement de reproduire le mécanisme de vision d’un organisme vivant.

Les difficultés rencontrées : outre le fait de devoir reconnaître des écritures manuscrites, il faut notamment gérer aussi les abréviations ou les sauts de page.

{{< /pnotec >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Apprendre aux machines à apprendre
{{< /pcache >}}
### 2.2. Qu'est-ce que le machine learning ?

> L'apprentissage automatique est un champ d'étude de l'intelligence artificielle qui se fonde sur des approches mathématiques et statistiques pour donner aux ordinateurs la capacité d'"apprendre" à partir de données, c'est-à-dire d'améliorer leurs performances à résoudre des tâches sans être explicitement programmés pour chacune. (Source : [Wikipédia](https://fr.wikipedia.org/wiki/Apprentissage_automatique))

**L'enjeu est d'entraîner des modèles.**

{{< pnote >}}
Deux phases de l'apprentissage machine : estimer un modèle et appliquer ce modèle.

Attention de bien prendre en compte un enjeu important ici : le _machine learning_ a un intérêt pour traiter des lots importants de données.
Dans le cas de la numérisation il s'agit d'un travail long de préparation qui ne vaut la peine que si le corpus a numérisé est conséquent.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Apprendre aux machines à apprendre
{{< /pcache >}}
### 2.3. Modéliser ?

- effectuer le même travail que la machine
- disposer d'exemples suffisamment divers
- partager ces ressources

{{< pnote >}}
Pour qu'une machine _apprenne_ il faut qu'elle dispose de données structurées et modélisées.

{{< /pnote >}}
{{< psectiono >}}


