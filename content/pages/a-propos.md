---
title: À propos
date: 2022-09-01
---
Bienvenue sur le site web du cours HNU2000 Humanités numériques : technologies.

Ce site regroupe les contenus du cours, il est destiné aux étudiant·e·s qui suivent le cours à l'automne 2022. Les contenus sont placés sous la licence CC-BY-NC-SA.

Contact : Antoine Fauchié, [antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca)
