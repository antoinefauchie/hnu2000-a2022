---
title: "Évaluation de fin de session"
date: 2022-12-07
#layout: evaluation
correction: false
---

## Instructions

Cette évaluation de fin de session est un travail individuel de rédaction à réaliser hors salle du jeudi 8 décembre 2022 au vendredi 16 décembre 2022 à 23h59.
L'accès aux ressources du cours est autorisé et même fortement recommandé, ainsi qu'aux lectures obligatoires et à la bibliographie.
Attention toutefois à respecter les règles en matière de fraude et de plagiat, aucune citation non indiquée ou non référencée ne sera autorisée.

**Un seul des 3 sujets suivants devra être choisi et traité dans une réponse structurée (introduction, annonce d'un plan, plusieurs parties articulées comprenant arguments et exemples, références) comprise entre 8000 et 12000 signes (entre 1000 et 1600 mots), en utilisant les ressources du cours (supports, lectures obligatoires, exposés et bibliographie).**

Critères de notation :

- orthographe et grammaire ;
- qualité de la rédaction ;
- clarté du texte dans son ensemble (privilégier des phrases compréhensibles à des figures de styles) ;
- argumentation de vos analyses :
- structure de votre texte (parties, sous-parties) ;
- illustrations de vos propos : descriptions, images, etc. ;
- utilisation du cours ;
- usage des références bibliographiques/lectures du cours (et pertinence de cet usage).

<div class="break"></div>

## Sujet 1 : comparaison de deux approches en humanités numériques

Choisissez deux outils ou technologies issus des humanités numériques — et pas forcément dans le même domaine — pour présenter deux approches possibles, divergentes ou convergentes.
Votre proposition permettra de montrer la diversité qu'il existe dans les humanités numériques, diversité qui se traduit aussi par des choix techniques spécifiques.


## Sujet 2 : définition commentée

Donner une définition aussi complète que possible des "humanités numériques" comprenant au moins 3 points distincts.
Chacun de ces points doit être explicité avec des références précises, vous devez également illustrer vos propos avec des présentations détaillées d'outils.


## Sujet 3 : l'enjeu des standards

En quoi la question des standards peut être une question centrale des humanités numériques ?
Après avoir défini ce qu'est un standard, vos arguments critiques devront s'appuyer sur plusieurs technologies et outils.

