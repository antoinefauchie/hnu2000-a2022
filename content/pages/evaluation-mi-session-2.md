---
title: Évaluation de mi-session
date: 2022-11-02
layout: evaluation
correction: true
---
<!--
## Instructions
Cet examen de mi-session se déroule jeudi 3 novembre 2022 dans la salle du pavillon Lionel Groulx.

Ce devoir sur table doit être réalisé sans les supports de cours ou tout type de documents.

<div class="break"></div>
-->
## Partie 1 : questions courtes
<!--Ces questions demandent des réponses synthétiques de quelques phrases.-->

### Quelles sont les trois principales particularités des humanités numériques ? (8 points)
Les humanités numériques se distinguent par trois particularités (liste non exhaustive) :

1. il s'agit d'une approche interdisciplinaire et non d'un domaine scientifique ;
2. les humanités numériques sont la conjonction des humanités et du numérique, et non une addition ;
3. cette approche nous oblige à nous interroger sur notre utilisation des outils numériques en particulier et sur la technologie en général.

### Citer trois outils représentatifs des humanités numériques en les décrivant en quelques mots (8 points)
Voici trois outils qui représentent ce que peuvent être les humanités numériques :

- Isidore.science : un moteur de recherche spécialisé pour les sciences humaines, basés sur les standards du web sémantique et qui permet de réunir de nombreuses ressources ;
- Voyant Tools : un outil d'analyse de textes qui offrent plusieurs fonctionnalités d'extraction de termes, de calculs statistiques et de visualisation graphique ;
- Zotero : un outil de gestion de références bibliographiques qui offrent de nombreuses fonctionnalités en plus de l'enregistrement et de l'organisation de références bibliographiques.

### Expliquer en quelques phrases comment fonctionne un moteur de recherche (8 points)
Un moteur de recherche a un fonctionnement spécifique qui peut être résumé en quelques étapes : indexation de pages web grâce à des robots qui parcourent le web ; classement des pages selon plusieurs algorithmes ; traitement d'une requête en temps réel pour apporter le résultat le plus pertinent selon des critères préétablis et avec l'aide d'algorithmes ; affichage des résultats selon des critères de classement.

### Qu'est-ce que le format texte ? (8 points)
Le format texte, aussi appelé texte brut (ou _plain text_ en anglais), est un format informatique de fichier qui ne comporte rien d'autres que des caractères textuels et qui peut être lu directement via un éditeur de texte.
Le format texte est lisible, léger, pérenne et dans une certaine mesure interopérable.

### Lister au moins trois contraintes de l'édition scientifique en donnant quelques éléments de présentation pour chacune d'elles (8 points)
Les contraintes de l'édition scientifique sont nombreuses, voici trois d'entre elles rapidement présentées :

1. les textes comportent un important appareil critique : notes, bibliographie, structuration spécifique. Les textes scientifiques sont sémantiquement riches ;
2. les documents et les textes circulent entre plusieurs étapes et personnes : les textes sont sujet à de nombreuses interventions, depuis les réviseurs jusqu'aux graphistes en passant par les éditeurs·trices ;
3. la diffusion est un enjeu majeur : l'objectif est de pouvoir diffuser largement ou tout du moins sur des espaces bien identifiés, et cela signifie devoir produire un ou plusieurs formats compatibles avec différentes plateformes de diffusion.

<div class="break"></div>

## Partie 2 : questions longues
<!--Les questions qui suivent demandent des réponses plus longues de plusieurs lignes, vous devez être précis et synthétiques.
Les références au cours et aux lectures du cours sont attendues (le titre ou l'auteur·trice d'un document est suffisant).-->

### Proposer une définition des humanités numériques en vous appuyant sur les premières séances et les textes vus en cours (vous pouvez citer des titres ou des noms d'auteurs·trices sans les références complètes) (20 points)
Les humanités numériques peuvent être considérées comme une approche pluridisciplinaire en sciences humaines, elles ont la particularité de proposer des méthodes spécifiques, l'usage de l'informatique ainsi que la création d'outils pour des besoins de recherche.
L'approche des humanités numériques est aussi une manière d'interroger la technologie qui est invoquée dans les processus de recherche.
Pour paraphraser Thomas Lebarbé, il s'agit de l'utilisation d'outils, de médias et de corpus numériques dans les sciences humaines, modifiant ainsi à la fois les objets et les pratiques dans les sciences humaines.
Michael Sinatra et Marcello VItali-Rosati résument bien l'ambiguïté des humanités numériques dans _Pratiques de l'édition numérique_ : "le numérique est-il le sujet ou l'objet des humanités numériques ?"

### En vous appuyant sur un outil présenté en cours, expliquer en quoi l'usage de l'informatique ou du numérique permet de faire de la recherche en sciences humaines autrement (20 points)
Le recours à l'informatique ou au numérique est un atout pour les sciences humaines, par exemple avec des outils d'analyse ou de fouille de textes comme Ngram Viewer ou Voyant Tools.
Plutôt que de chercher _à la main_ les occurrences de termes ou d'expressions, des logiciels ou des programmes informatiques peuvent effectuer ce type de recherche plus rapidement et sur de plus grands corpus.
Il ne s'agit pas que d'un gain de temps, puisque l'écriture des algorithmes de recherche permet aussi de modéliser des questions théoriques et ainsi de formaliser des processus de recherche.
L'enjeu ici est donc triple : obtenir de nouveaux résultats scientifiques impossibles sans le numérique, interroger les usages de la technologie dans les pratiques scientifiques, et enfin concevoir ses propres outils pour faire de la recherche.

### À partir des outils présentés en cours, expliquer pourquoi et comment la communauté scientifique développe ses propres outils (20 points)
Stylo est un éditeur de texte sémantique pour l'écriture et l'édition scientifique en sciences humaines, cet outil a été créé pour palier aux logiciels de traitement de texte comme Word qui ne répondent pas aux besoins de la communauté scientifique.
Stylo est une réponse parmi d'autres aux problèmes imposés par Word, c'est un projet coordonné par une communauté scientifique et financé par des structures publiques.
Zotero est un logiciel libre (et donc gratuit) de gestion de références bibliographiques.
Il a été créé par et pour des chercheurs·res, son développement ouvert permet de recueillir des demandes d'évolution et de s'adapter aux normes bibliographiques sans dépendre de la vente de licences.
Le développement de Zotero est possible car une équipe de scientifiques est impliquée dans le projet et parce que la structure qui le porte reçoit des financements publics.

