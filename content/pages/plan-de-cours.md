---
title: Plan de cours
date: 2022-09-08
bibfile: data/bibliographie.json
---
**Cours en présentiel les jeudis de 8h30 à 11h30 dans la salle 415 au 3744 avenue Jean Brillant.**

Antoine Fauchié — [antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca)  
Présent dans les locaux C-8041 ou C-8132 les mardis et mercredis de 10h à 16h (merci de m'écrire avant de venir me voir pour vous assurer que je sois là).

## Note importante
Ce cours est programmé en présentiel à partir du jeudi 8 septembre 2022, les étudiant·e·s doivent donc être présent·e·s à toutes les séances tant que les conditions le permettent.
Ce plan de cours est susceptible d'évoluer, les étudiant·e·s seront prévenu·e·s au fur et à mesure.  
**Salle 415 au 3744 avenue Jean Brillant (5 minutes à pied du Pavillon Lionel Groulx ou Jean Brillant.**

## Présence et participation
Si votre présence est fortement recommandée à chaque séance, nous savons désormais qu'il est difficile de prévoir l'imprévisible.
La présence n'est pas évaluée mais je note qui est présent en classe.
En suivant ce cours vous vous engagez toutefois à venir autant que possible à chaque séance et à suivre les recommandations suivantes dans la mesure du possible et de votre situation :

- préparation : lectures demandées et relecture du cours ;
- concentration : éviter les distractions pendant les séances ;
- être actif·ve : écouter le cours, participer aux manipulations, poser des questions ;
- écouter : lorsque d'autres étudiant·e·s prennent la parole, écoutez-les ;
- créer des connexions : synthétiser et mettre en rapport les différents contenus présentés.

## 1. Objectifs et contenus
Que sont les humanités numériques et quelles technologies sont utilisées dans cette approche scientifique ?
Les "humanités numériques" sont à la fois une méthode scientifique, un programme de recherche ou une approche pluridisciplinaire : elles offrent de nouvelles perspectives pour appréhender, lire et comprendre le monde qui nous entoure.
Depuis l'avènement du numérique, la compréhension des écosystèmes technologiques devient une nécessité dans le champ des sciences humaines.
Le cours HNU 2000 "Humanités numériques : technologies" est une opportunité pour explorer de façon originale les outils théoriques et pratiques utilisés dans les humanités numériques : lire, écrire, chercher, explorer, visualiser, analyser, publier, etc.
Ce cours est un espace de formation pratique et de découverte des méthodes de base en humanités numériques, avec une orientation vers les démarches d'écriture, d'édition et de publication.

Ce cours est une initiation aux technologies utilisées dans les humanités numériques : les étudiant·e·s devront comprendre, explorer, manipuler et expérimenter des outils, des logiciels, des méthodes et des programmes informatiques.
Le cours HNU 1000 "Humanités numériques : théories" est un compagnon de ce cours pratiques.

À l'issue du cours les étudiant·e·s seront en mesure de comprendre les enjeux technologiques des humanités numériques, de réutiliser des concepts liés aux humanités numériques, d'appréhender des méthodes utilisées dans les humanités numériques et d'utiliser des outils/applications.

---

## 2. Bibliographie
Cette bibliographie comprend des lectures obligatoires (textes indiqués au moins une semaine avant sur Studium et sur le site web dédié au cours), des lectures recommandées (citées en cours) et des sources supplémentaires (à consulter pour compléter certaines parties du cours).

{{< bibliography >}}

---

## 3. Évaluations
Plusieurs évaluations sont prévues au cours de la session :

1. examen de mi-session (30 %) : questionnaire lors de la séance du 20 octobre 2022 ;
2. étude de cas sous forme d'exposé (30 %) : cours exposé de 10 minutes en classe pour présenter une méthode ou un outil issu des humanités numériques ;
3. examen de fin de session (40%) : rédaction (environ 1300 mots / 10000 signes) à partir d'un sujet donné.

### 3.1. Examen de mi-session (30 %)
L'examen se fera sur table dans la salle du cours jeudi 20 octobre 2022 à 8h30.
Les indications de cet examen seront données le jour même.

### 3.2. Étude de cas sous forme d'exposé (30 %)
Lors des premières semaines (jusqu'au 6 octobre) plusieurs projets, méthodes et outils issus des humanités numériques seront présentés brièvement, les étudiant·e·s devront en choisir un pour le présenter lors d'un exposé oral en classe à partir du 3 novembre.
Votre choix doit être conduit par un réel intérêt.
Voici les objectifs et les contraintes pour l'exposé :

- présenter clairement un objet ;
- partager un intérêt autour d'une initiative en humanités numériques ;
- structurer une présentation ;
- répondre aux questions pendant l'exposé ;
- affirmer une position tout en étant ouvert·e aux remarques ;
- temps imparti : 10 minutes pour l'exposé puis un temps d'échange avec l'enseignant et les étudiant·e·s ;
- support de présentation recommandé (évitez de mettre trop de textes sur les diapositives, privilégiez les images/schémas/dessins).

Et voici une proposition de plan à suivre ou dont vous pouvez vous inspirer (chaque élément doit être abordé pendant votre exposé) :

- **description courte** : quelques mots qui présentent brièvement l'objet étudié ;
- **contexte** : présentez les objectifs de cette initiative, pourquoi un tel projet/outil/méthode a été mis en place ? Pour qui et à quel fin ?
- **fonctionnement ou description technique** : selon l'objet étudié présentez le fonctionnement ou les détails techniques du projet/outil/méthode ;
- **comparaison avec d’autres initiatives** : effectuer une courte comparaison avec d'autres initiatives équivalentes ou proches en indiquant les points communs ou les éléments de différenciation ;
- **analyse critique** (avec l’appui de références issues du cours ou de la bibliographie) : à partir des points précédents, proposer un point de vue argumenté sur l'objet. À quels besoins répond-il ? Pourquoi présente-t-il un intérêt ? Est-ce un outil superflu ou incontournable dans un champ donné ? etc. ;
- **conclusion** : résumé synthétique de ce que vous venez de présenter avec une éventuelle ouverture.

Enfin voici les dates importantes pour cet exercice :

- jusqu'au 6 octobre : présentation par l'enseignant d'un certain nombre de projets, de méthodes ou d'outils pendant le cours (une liste sera mise à disposition) ;
- avant le 13 octobre : les étudiant·e·s doivent indiquer leur choix, c'est-à-dire sur quoi portera leur exposé ;
- le 20 octobre : dernier délai pour l'enseignant pour valider les sujets (si nécessaire une rencontre personnalisée peut être organisée) ;
- à partir du 3 novembre : début des exposés pendant le cours (les étudiant·e·s seront informés de leur ordre de passage pendant la semaine de lecture).

### 3.2. Rédaction (40 %)
Un sujet au choix doit faire l'objet d'un travail individuel de rédaction.
Les sujets sont disponibles à l'adresse suivante : [https://hnu2000.quaternum.net/evaluation-finale/](https://hnu2000.quaternum.net/evaluation-finale/)
Cette rédaction structurée doit comprise entre 8000 et 12000 signes (entre 1000 et 1600 mots), en utilisant les ressources du cours (supports, lectures obligatoires, exposés, bibliographie).

Critères de notation :

- orthographe et grammaire ;
- qualité de la rédaction ;
- clarté du texte dans son ensemble (privilégier des phrases compréhensibles à des figures de styles) ;
- argumentation de vos analyses :
- structure de votre texte (parties, sous-parties) ;
- illustrations de vos propos : descriptions, images, etc. ;
- utilisation du cours ;
- usage des références bibliographiques/lectures du cours (et pertinence de cet usage).

**D'autres consignes sont indiquées sur la page du sujet, merci de les lire avec attention.**

---

## 4. Séances
_Les séances sont susceptibles de changer en fonction du déroulement du cours._

- Séance 01 - 08-09-2022 - Introduction, définitions et principes des humanités numériques
- Séance 02 - 15-09-2022 - 10 outils des Humanités numériques
- Séance 03 - 22-09-2022 - Isidore.science / Chercher de l'information (Internet, moteurs de recherche)
- Séance 04 - 29-09-2022 - Hypothes.is / Annoter le Web (Open Access, standards)
- Séance 05 - 06-10-2022 - Stylo / Écrire sémantiquement (écriture numérique, balisages)
- Séance 06 - 13-10-2022 - Zotero / Récolter, organiser et partager des données bibliographiques (bibliographie structurée)
- Séance 07 - 20-10-2022 - Évaluation de mi-session
- Séance 08 - 03-11-2022 - Ngram Viewer / Comparer dans le temps (index, taxonomie)
- Séance 09 - 10-11-2022 - Voyant Tools / Analyser du texte (logiciel libre, fouille de texte)
- Séance 10 - 17-11-2022 - WordPress / Publier sur le web (WYSIWYG)
- Séance 11 - 24-11-2022 - Gephi / Visualiser des réseaux (logiciel libre, visualisation)
- Séance 12 - 01-12-2022 - eScriptorium / Numériser des documents manuscrits (numérisation, machine learning, communauté des DH)
- Séance 13 - 08-12-2022 - Ed. et GitHub Pages / Publier des contenus sans trop de dépendance (édition numérique, minimal computing, single source publishing)

---

## 5. Soutien et santé mentale
La rentrée 2022 est la première rentrée dans des conditions presque normale depuis 2019, nous avons éprouvé ou nous éprouvons peut-être encore malgré tout d'innombrables défis liés à la pandémie.
Je tente de vous permettre de suivre ce cours de façon normale et de rester le plus possible disponible pour discuter avec vous de vos difficultés dans ce cours ou dans vos études.
J'essaye de rester disponible par courriel ou certains jours à l'université (C-8041 ou C-8132 les mardis et mercredis de 10h à 16h), au besoin des services de santé mentale sont proposés par l'Université : http://www.cscp.umontreal.ca et https://www.cavaaller.ca/#services.
N'attendez pas quand vous rencontrez des difficultés, prenez contact avec moi ou d'autres personnes ressources à l'université.

---

## 6. Plagiat et fraude
"Tout acte de plagiat, fraude, copiage, tricherie ou falsification de document commis par une étudiante, un étudiant, de même que toute participation à ces actes ou tentative de les commettre, à l’occasion d’un examen ou d’un travail faisant l’objet d’une évaluation ou dans toute autre circonstance, constituent une infraction. Vous trouverez à l’adresse suivante les différentes formes de fraude et de plagiat ainsi que les sanctions prévues par l’Université : [https://integrite.umontreal.ca](https://integrite.umontreal.ca)"

