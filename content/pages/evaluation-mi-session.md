---
title: "Évaluation de mi-session : correction"
date: 2022-10-18
layout: evaluation
correction: true
---
<!--
## Instructions
Cet examen de mi-session se déroule jeudi 20 octobre 2022 dans la salle du cours "HNU 2000 Humanités numériques : technologies" : Salle 415, 3744 avenue Jean Brillant.

Ce devoir sur table doit être réalisé sans les supports de cours ou tout type de documents.

<div class="break"></div>
-->

## Partie 1 : questions courtes
<!-- Ces questions demandent des réponses synthétiques de quelques phrases. -->

### Quelle erreur est parfois faite en définissant les humanités numériques à partir de ses deux termes ? Préciser comment contourner ce problème d'interprétation (8 points)
Les humanités sont parfois définies comme l'adjonction de l'informatique aux sciences humaines, et c'est une erreur de considérer qu'il ne s'agit que d'une addition.
Pour contourner ce problème, il faut plutôt constater qu'il s'agit d'une conjonction : le numérique (ou l'informatique) et les humanités (ou les sciences humaines) se joignent, il y a une rencontre autant que le partage d'un parcours.
Cela se traduit notamment par la dimension réflexive des humanités numériques.

### Quel est le fonctionnement du PageRank et de quel autre principe s'inspire-t-il ? (8 points)
Le PageRank est l'un des algorithmes utilisés par le moteur de recherche Google pour classer les très nombreuses pages web qu'il indexe.
Le PageRank est une mesure qualitative des pages web selon le nombre de liens qui pointent vers elles, ces liens étant eux-mêmes sujets à la même mesure.
Ce principe s'inspire d'un autre principe, celui du Science Citation Index utilisé pour classer les articles scientifiques.

### Qu'est-ce que le standard HTML ? (8 points)
Le standard HTML est une suite de spécificités techniques définissant le fonctionnant du langage informatique HTML (pour HyperText Markup Language).
Il s'agit de balises sémantiques permettant de qualifier l'information, pour ensuite être interprétées et rendues graphiquement à l'écran par un navigateur web.
Ces éléments sont nombreux et parfois liés les uns aux autres — ils sont imbriqués.
Un langage standard permet de définir des règles communes de fonctionnement, grâce à un standard il est par exemple possible de créer un logiciel qui affiche des pages web (comme un navigateur web).

### Quelle est l'origine des traitements de texte ? (8 points)
L'origine des traitements de texte est intéressante, puisque c'est un développeur qui a pensé à créer ce type de logiciel.
Alors que les développeurs·peuses écrivaient la documentation de leur code sur une machine à écrire et non sur un ordinateur, Michael Shraver a développé Electric Pencil au milieu des années 1970 pour pouvoir réaliser ces deux tâches sur un même dispositif, plutôt que sur deux machines séparées.

### Lister au moins trois contraintes de l'édition scientifique en donnant quelques éléments de présentation pour chacune d'elles (8 points)
Les contraintes de l'édition scientifique sont nombreuses, voici trois d'entre elles rapidement présentées :

1. les textes comportent un important appareil critique : notes, bibliographie, structuration spécifique. Les textes scientifiques sont sémantiquement riches ;
2. les documents et les textes circulent entre plusieurs étapes et personnes : les textes sont sujet à de nombreuses interventions, depuis les réviseurs jusqu'aux graphistes en passant par les éditeurs·trices ;
3. la diffusion est un enjeu majeur : l'objectif est de pouvoir diffuser largement ou tout du moins sur des espaces bien identifiés, et cela signifie devoir produire un ou plusieurs formats compatibles avec différentes plateformes de diffusion.

<div class="break"></div>

## Partie 2 : questions longues
Les questions qui suivent demandent des réponses plus longues de plusieurs lignes, vous devez être précis et synthétiques.
Les références au cours et aux lectures du cours sont attendues (le titre ou l'auteur·trice d'un document est suffisant).

### Proposer une définition des humanités numériques en vous appuyant sur les premières séances et les quatre outils présentés en cours (20 points)
Les humanités numériques peuvent être considérées comme une approche pluridisciplinaire en sciences humaines, elles ont la particularité de proposer des méthodes spécifiques, l'usage de l'informatique ainsi que la création d'outils pour des besoins de recherche.
L'approche des humanités numériques est aussi une manière d'interroger la technologie qui est invoquée dans les processus de recherche.
Avec des outils comme Isidore, Hypothesis, Stylo ou Zotero, l'usage de la technique est fait pour faciliter la recherche scientifique et sa diffusion.
Mais ce n'est pas tout : il s'agit aussi, en proposant ces outils, de questionner le recours que nous avons habituellement à des logiciels qui ne sont pas pensés pour la recherche scientifique ou proposés par des entreprises commerciales.

### Expliquer comment les Communs peuvent contribuer à une plus grande circulation de la connaissance (vous pouvez donner des exemples vus en cours) (20 points)
Les Communs sont des ressources partagées, maintenues et gérées de façon collective par une communauté, il s'agit de les préserver (par exemple des ressources naturelles) et de les diffuser largement (par exemple des ressources documentaires ou culturelles).
Avec les Communs vient une dimension d'ouverture afin de faciliter la collaboration : il est nécessaire de permettre l'accès le plus large possible afin de favoriser les contributions.
En considérant certaines ressources comme faisant partie des Communs, il devient alors possible d'étendre le champ des possibles, puisque ces ressources seront partagées et qu'un plus grand nombre de personnes pourra y accéder, en prendre soin ou y contribuer.
En considérant par exemple les productions scientifiques comme des communs, la diffusion de ces contenus peut être beaucoup plus large : toute personne peut y accéder et les diffuser à son tour.
En ouvrant l'accès aux productions scientifiques, il devient aussi possible d'interagir sur ces documents, par exemple en les annotant.


### À partir des outils présentés en cours, présenter plusieurs enjeux liés au développement de logiciels ou services proposés par la communauté scientifique (20 points)
L'approche pluridisciplinaire des humanités numériques a une conséquence : l'utilisation, le test, la critique, la modification ou la production d'outils par et pour la communauté scientifique.
Il est fréquent que les chercheur·e·s améliorent ou créent les outils dont ils ont besoin pour réaliser leur recherche.
Cela implique tout d'abord que ces outils puissent être modifiés, parce que la licence le permet et parce que le code source est disponible.
Ce n'est pas le cas de logiciels propriétaires dont le code est protégé : il n'est pas possible d'étudier ce code parce que l'accès est interdit (il est légalement impossible de modifier le code et donc le comportement du logiciel) ou tout simplement indisponible (le code n'est pas mis à disposition sur un dépôt public par exemple).
Nous pouvons prendre l'exemple des traitements de texte qui ne contiennent pas (par défaut) de fonctionnalités de gestion des citations et des bibliographies.
Si ces outils pouvaient être modifiés et adaptés à des usages plus spécifiques, alors la communauté serait gagnante.
Zotero permet de recevoir autant des demandes de modifications que des modifications directes.
Dans le cas où des chercheur·e·s, étudiant·e·s ou d'autre membres de la communauté scientifique créent leur propre outil, il est nécessaire de permettre à d'autres de contribuer à ce travail : en modifiant le fonctionnement du logiciel (en faisant une copie par exemple) ou en proposant des pistes de modification.


