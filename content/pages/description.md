---
title: Description
date: 2022-09-01
bibfile: data/bibliographie-courte.json
---
HNU 2000 Humanités numériques : technologies  
Automne 2022, le jeudi de 8h30 à 11h30

## 1. Objectifs et contenu du cours
Que sont les humanités numériques et quelles technologies sont utilisées dans cette approche scientifique ?
Les "humanités numériques" sont à la fois une méthode scientifique, un programme de recherche ou une approche pluridisciplinaire : elles offrent de nouvelles perspectives pour appréhender, lire et comprendre le monde qui nous entoure.
Depuis l'avènement du numérique, la compréhension des écosystèmes technologiques devient une nécessité dans le champ des sciences humaines.
Le cours HNU 2000 "Humanités numériques : technologies" est une opportunité pour explorer de façon originale les outils théoriques et pratiques utilisés dans les humanités numériques : lire, écrire, chercher, explorer, visualiser, analyser, publier, etc.
Ce cours est un espace de formation pratique et de découverte des méthodes de base en humanités numériques, avec une orientation vers les démarches d'écriture, d'édition et de publication.

Ce cours est une initiation aux technologies utilisées dans les humanités numériques, les étudiant·e·s devront comprendre, explorer, manipuler et expérimenter des outils, des logiciels, des méthodes et des programmes informatiques.
Le cours HNU 1000 "Humanités numériques : théories" est à la fois un prérequis et un compagnon de ce cours pratiques.

À l'issue du cours les étudiant·e·s seront en mesure de comprendre les enjeux technologiques des humanités numériques, de réutiliser des concepts liés aux humanités numériques, d'appréhender des méthodes utilisées dans les humanités numériques et d'utiliser des outils/applications.

## 2. Liste (provisoire et indicative) des textes à l'étude
Les textes étudiés seront mis à disposition.
D'autres textes seront ajoutés pour les lectures en cours.

{{< bibliography >}}

## 3. Organisation du cours
L'ordinateur portable des étudiant·e·s sera requis pour certaines séances.
Le recours à des applications en ligne (applications web) sera privilégié mais des logiciels devront parfois être installés.

Plusieurs technologies seront présentées avec à chaque fois :

- une contextualisation théorique en lien avec le cours HNU 1000 ;
- une découverte pratique pendant et hors des séances ;
- une expérimentation de cette technologie en lien avec des projets de recherche scientifique ;
- une présentation commune professeur et étudiant·e·s, en cours, des résultats constatés.

Chaque séance sera ponctuée par différents temps :

- commentaire critique et collectif d'une lecture ;
- présentation théorique ;
- manipulation d'une technologie ;
- discussion sur les enjeux techniques.

<div class="break"></div>

## 4. Modalités d'évaluation prévues
Plusieurs évaluations sont prévues pendant la session :

- la réalisation d'un devoir sur table (questionnaire avec questions courtes et longues), travail de mi-session : 30% ;
- une étude de cas sur une méthode ou un outil issu des humanités numériques sous la forme d'un exposé oral en cours : 30% ;
- la réalisation d'une étude de cas d'un outil/d'une technologie, travail de fin de session : 40%.
